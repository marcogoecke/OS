# Local IspellDict: en
#+SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+STARTUP: showeverything
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="./reveal.js/dist/theme/index.css" />
#+TITLE: Open Educational Resource (OER) presentations for a course on Operating Systems
#+AUTHOR: Jens Lechtenbörger
#+OPTIONS: html-style:nil
#+OPTIONS: toc:nil
#+KEYWORDS: operating systems, OER, open educational resources, creative commons, free and open license, presentations, slides, JiTT, just-in-time teaching
#+DESCRIPTION: Collection of OER presentations (HTML slides and PDF documents) for university course on Operating Systems with teaching and learning strategy JiTT

* What is this?
This page collects
[[https://en.wikipedia.org/wiki/Open_educational_resources][OER]]
presentations (HTML slides with embedded audio and PDF variants, available under
[[https://en.wikipedia.org/wiki/Free_license][free]] and open
[[https://creativecommons.org/about/cclicenses/][Creative Commons licenses]])
for a course on Operating Systems (following
the book [[https://gustavus.edu/mcs/max/os-book/][Operating Systems and Middleware: Supporting Controlled Interaction]]
by Max Hailperin) as part of the module
[[https://www.wi.uni-muenster.de/student-affairs/course-offerings/261893][Computer Structures and Operating Systems]]
for 4th-term students in the Bachelor program in
[[https://www.wi.uni-muenster.de/][Information Systems]]
at the [[https://www.uni-muenster.de/en/][University of Münster, Germany]].
Some background concerning the origin of this project and the adopted
teaching and learning strategy Just-in-Time Teaching
(JiTT; see [[https://en.wikipedia.org/wiki/Just_in_Time_Teaching][here for the Wikipedia entry]])
is explained in its [[https://gitlab.com/oer/OS/blob/master/README.md][README]].

If you are teaching or learning Operating Systems (OSs), feel free to
use, share, and adapt my presentations.  As usual for projects on GitLab,
you can open [[https://docs.gitlab.com/ee/user/project/issues/][issues]]
to report bugs or suggest improvements, ideally
with [[https://docs.gitlab.com/ee/user/project/merge_requests/][merge requests]].
The document
[[https://gitlab.com/oer/OS/blob/master/CONTRIBUTING.org][CONTRIBUTING]]
contains some pointers for contributors.

Students in Münster have taken courses on programming in Java, data
structures and algorithms, and data modeling before taking “Computer
Structures and Operating Systems.”  As *prerequisite* for Operating
Systems, students should
- be able to write, compile, and execute small /Java/ programs,
- be able to use basic /data structures/ (stack, queue, tree) and
  /algorithms/ (in particular, hashing);
- also, being able to explain the database /transaction concept/ and
  /update anomalies/ will ease understanding of presentations on
  mutual exclusion.

*Learning objectives*: After taking this course, students will be able
- to discuss major architectures and components of modern OSs; to
  explain and contrast processes and threads and their roles for OSs
  and applications,
- to explain OS data structures, algorithms, and management techniques,
- to analyze programming challenges arising from concurrency
  and to apply appropriate techniques addressing these challenges,
- to discuss the notion of IT security and to apply security
  mechanisms provided by the operating system in support of
  secure IT systems.

* Presentations
  :PROPERTIES:
  :CUSTOM_ID: presentations
  :END:
   *Note*: OER presentations (HTML slides and PDF documents) linked
   here are generated automatically from
   their source files (with the FLOSS software
   [[https://gitlab.com/oer/emacs-reveal][emacs-reveal]]).
   Presentations and source files are published under the Creative
   Commons license
   [[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]].
   Files are updated throughout the term (and thereafter).
   The PDF versions provided below are generated via LaTeX from Org
   source files.

   # *WARNING!* Presentations below will be updated for use in 2022 during
   # the term.  Presentations are “ready” if a link in Learnweb exists.

   #+ATTR_HTML: :about https://oer.gitlab.io/OS/index.html :prefix schema: http://schema.org/
   - [[hasPart:index-terms.org][*List of index terms for Operating Systems*]]
     (may simplify search for specific topics).
   - [[hasPart:CSOS-learning.org][*Learning and Teaching* (CSOS)]]
   - [[hasPart:Operating-Systems-Motivation.org][*OS Motivation*]] ([[file:Operating-Systems-Motivation.pdf][PDF]])
   - [[hasPart:Operating-Systems-Overview.org][*OS Overview*]] ([[file:Operating-Systems-Overview.pdf][PDF]])
   - Self-study presentations
     - [[hasPart:Operating-Systems-Introduction.org][*OS Introduction*]] ([[file:Operating-Systems-Introduction.pdf][PDF]])
     - [[hasPart:Operating-Systems-Interrupts.org][*Interrupts*]] ([[file:Operating-Systems-Interrupts.pdf][PDF]])
     - [[hasPart:Operating-Systems-Threads.org][*Threads*]] ([[file:Operating-Systems-Threads.pdf][PDF]])
     - [[hasPart:Operating-Systems-Scheduling.org][*Scheduling*]] ([[file:Operating-Systems-Scheduling.pdf][PDF]])
     - [[hasPart:Operating-Systems-MX.org][*Mutual Exclusion (MX)*]] ([[file:Operating-Systems-MX.pdf][PDF]])
     - [[hasPart:Operating-Systems-MX-Java.org][*MX in Java*]] ([[file:Operating-Systems-MX-Java.pdf][PDF]])
     - [[hasPart:Operating-Systems-MX-Challenges.org][*MX Challenges*]] ([[file:Operating-Systems-MX-Challenges.pdf][PDF]])
     - [[hasPart:Operating-Systems-Memory-I.org][*Virtual Memory I*]] ([[file:Operating-Systems-Memory-I.pdf][PDF]])
     - [[hasPart:Operating-Systems-Memory-Linux.org][*Virtual Memory with Linux*]] ([[file:Operating-Systems-Memory-Linux.pdf][PDF]])
     - [[hasPart:Operating-Systems-Memory-II.org][*Virtual Memory II*]] ([[file:Operating-Systems-Memory-II.pdf][PDF]])
     - [[hasPart:Operating-Systems-Processes.org][*Processes*]] ([[file:Operating-Systems-Processes.pdf][PDF]])
     - [[hasPart:Operating-Systems-Security.org][*Security*]] ([[file:Operating-Systems-Security.pdf][PDF]])
   - Older presentations
     - 2020: [[hasPart:Operating-Systems-JiTT.org][*JiTT for OS*]] ([[file:Operating-Systems-JiTT.pdf][PDF]])

   For offline work (also on mobile devices), you can
   [[https://gitlab.com/oer/OS/-/jobs/artifacts/master/download?job=pages][download the results of the latest pipeline execution on GitLab]]
   or clone the [[https://gitlab.com/oer/OS][source repository including all necessary resources]]
   and generate presentations yourself.

* Source code and licenses
#+MACRO: gitlablink [[https://gitlab.com/oer/OS/][$1]]
#+INCLUDE: "~/.emacs.d/oer-reveal-org/source-code-licenses.org"
