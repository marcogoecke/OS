# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config-course.org"
#+MACRO: jittquiz [[https://sso.uni-muenster.de/LearnWeb/learnweb2/course/view.php?id=60751#section-10][Learnweb]]

#+TITLE: OS06: MX in Java
#+SUBTITLE: Based on Chapter 4 of cite:Hai19
#+KEYWORDS: mutual exclusion, Java, monitor, synchronized
#+DESCRIPTION: First of three presentations on mutual exclusion in the context of operating systems.  Introduces mutual exclusion with monitors in Java (synchronized, wait(), notify())

* Introduction

# This is the seventh presentation for this course.
#+CALL: generate-plan(number=7)

** Today’s Core Question
   - How to achieve MX with monitors in Java?
   - (Also: Semaphores revisited with Java as alternative)

** Learning Objectives
   - Apply and explain MX and cooperation based on monitor concept in Java
     - Give example
     - Discuss incorrect attempts

** Retrieval Practice
*** Thread Terminology
#+REVEAL_HTML: <script data-quiz="quizThreadTerminology" src="./quizzes/thread-terminology.js"></script>

*** Thread States
#+REVEAL_HTML: <script data-quiz="quizThreadStates" src="./quizzes/thread-states.js"></script>

*** Java Threads
#+REVEAL_HTML: <script data-quiz="quizJavaThreads" src="./quizzes/java-threads.js"></script>

*** Races
#+REVEAL_HTML: <script data-quiz="quizRaces" src="./quizzes/races.js"></script>

*** Mutual Exclusion
#+REVEAL_HTML: <script data-quiz="quizMX" src="./quizzes/mx.js"></script>

** Table of Contents
   :PROPERTIES:
   :UNNUMBERED: notoc
   :HTML_HEADLINE_CLASS: no-toc-progress
   :END:
#+REVEAL_TOC: headlines 1

* Monitors
** Monitor Idea
   :PROPERTIES:
   :CUSTOM_ID: monitor-idea
   :reveal_extra_attr: data-audio-src="./audio/6-monitors.ogg"
   :END:
   #+INDEX: Monitor!Idea (MX II)
   - Monitor ≈ instance of class with methods and attributes
   - Equip *every* object (= class instance) with a lock
     - *Automatically*
       - Call ~lock()~ when method is entered
         - As usual: Thread is blocked if lock is already locked
         - Thus, automatic MX
         - We say that executing thread *entered the monitor* or
           *executes inside the monitor* when it has passed ~lock()~ and
           executes a method
       - Call ~unlock()~ when method is left
         - Thread *leaves the monitor*
   #+begin_notes
The basic idea of monitors is as follows: Think of a monitor as an
instance of a special type of class, where each instance is
automatically equipped with its *own lock*.  The run-time system ensures
that before a method of such a class is executed on a class instance
(which is ~this~ in Java), the lock for that class instance needs to
be acquired.

We say that a thread that has successfully executed ~lock()~ “entered
the monitor” or “executes inside the monitor”.

Thus, monitors automatically provide MX for methods of the monitor
class: If multiple threads share the same object (with a potential for
race conditions), only one of them can execute inside the monitor at
any point in time, while others are blocked.

Importantly, each object has its own lock.  Thus, two threads that
operate on different class instances can both acquire their different
locks and execute monitor methods in parallel (without the danger of
races as they do not not share resources).

The next slide explains the origin of monitors in terms of an abstract
data type (instead of the more modern “class” formulation presented
here).  On that slide, you also see that monitors not only guarantee
MX; in addition, they provide methods for cooperation of threads.

Subsequent slides then discuss how the monitor concept is implemented
in Java with the keyword ~synchronized~ (which activates locking of
the ~this~ object as explained here in general terms) and methods for
cooperation.
   #+end_notes

** Monitor Origin
   :PROPERTIES:
   :CUSTOM_ID: monitor-origin
   :END:
   #+INDEX: Monitor!Definition (MX II)
   - Monitors proposed by [[https://en.wikipedia.org/wiki/Tony_Hoare][Hoare]]; 1974
   - *Abstract data type*
     #+ATTR_REVEAL: :frag (appear)
     - Methods encapsulate local variables
       - Just like methods in Java classes
     - Thread enters monitor via method
       - *Built-in MX*: At most one thread in monitor
     - In addition: Methods for cooperation
       - ~cwait(x)~: Blocks calling thread until ~csignal(x)~
         - Monitor free then
       - ~csignal(x)~: Starts at most one thread waiting for x
	 - If existing; otherwise, nothing happens
	 - Difference to semaphore: signal may get lost

* MX in Java
** Monitors in Java: Overview
   :PROPERTIES:
   :CUSTOM_ID: java-monitor-intro
   :END:
   #+INDEX: Monitor!Java overview (MX II)
   - In Java, classes and objects come with built-in locks
     - Which are ignored by default
   - Keyword ~synchronized~ activates locks
     - Automatic locking of ~this~ object during execution of method
       - Automatic MX for method’s body
       - Useful if (large part of) body is a CS
     - E.g., for sample code from cite:Hai19 (for which you
       [[file:Operating-Systems-MX.org::#sell-race][found races]] previously):
       #+BEGIN_SRC java
public synchronized void sell() {
  if (seatsRemaining > 0) {
    dispenseTicket();
    seatsRemaining = seatsRemaining - 1;
  } else displaySorrySoldOut();
}
       #+END_SRC

*** Java, ~synchronized~, ~this~
    :PROPERTIES:
    :CUSTOM_ID: synchronized-this
    :END:
    - Java basics, hopefully clear
      - Method ~sell()~ from previous slides invoked on some object, say ~theater~
	- Each ~theater~ has its own attribute ~seatsRemaining~
	- ~seatsRemaining~ is really ~this.seatsRemaining~,
          which is the same as ~theater.seatsRemaining~
	  - Inside the method, the name ~theater~ is unknown, ~theater~
            is the ~this~ object, which is used implicitly
    #+ATTR_REVEAL: :frag appear
    - Without ~synchronized~, races arise when two threads invoke
      ~sell()~ on the same object ~theater~
      - With ~synchronized~, only one of the threads obtains the lock
        on ~theater~, so races are prevented

*** Possible Sources of Confusion
    :PROPERTIES:
    :CUSTOM_ID: possible-confusion
    :END:
    #+ATTR_REVEAL: :frag (appear)
    - With ~synchronized~, *locks* for *objects* are activated
      - For ~synchronized~ methods, thread needs to acquire lock for ~this~ object
    - Methods *cannot* be locked
    - Individual attributes of the ~this~ object (e.g.,
      ~seatsRemaining~) are *not* locked
      - (Which is not a problem as object-orientation recommends to
        *encapsulate* attributes, i.e., they cannot be accessed
        directly but only through ~synchronized~ methods)

*** JiTT Assignment
    :PROPERTIES:
    :CUSTOM_ID: jitt-synchronized-sell
    :reveal_data_state: no-toc-progress
    :reveal_extra_attr: class="jitt"
    :END:

    1. Inspect and understand, compile, and run [[basic:https://gitlab.com/oer/OS/blob/master/java/TheaterEx.java][this sample program]],
       which embeds the code to sell tickets, for which you
       [[file:Operating-Systems-MX.org::#sell-race][found races previously]].

    2. Change ~sell()~ to use the monitor concept, recompile, and run
       again.  Observe the expected outcome.

    (Nothing to submit here; maybe ask questions online.)

** Java Monitors in Detail
   :PROPERTIES:
   :CUSTOM_ID: java-monitors
   :reveal_extra_attr: data-audio-src="./audio/6-java-mx-1.ogg"
   :END:
   #+INDEX: Monitor!Java details (MX II)
   - MX based on [[#monitor-origin][monitor concept]]
     - See
       [[beyond:https://docs.oracle.com/javase/specs/jls/se18/html/jls-17.html][Java specification]]
       if you are interested in details
     #+ATTR_REVEAL: :frag appear :audio ./audio/6-java-mx-2.ogg
     - *Every* Java object (and class) comes with
       - Monitor with *lock* (not activated by default)
         - Keyword ~synchronized~ activates lock
         - For method
           - ~public synchronized methodAsCS(...) {…}~
           - Thread acquires *lock* for *[[#synchronized-this][this]]* object upon call (Class
             object for static methods)
         - Or for block
           - ~synchronized (syncObj) {…}~
           - Thread acquires *lock* for ~syncObj~
         - First thread *acquires lock* for duration of method/block
         - Further threads get *blocked*
       - *Wait set* (set of threads; ~wait()~ and ~notify()~, explained
         [[#cooperation-idea][later]]; ignore for now)
#+BEGIN_NOTES
1. Java provides all methods for mutual exclusion discussed in the
   previous presentation, including the monitor concept, whose details
   can be found at the URL given here.

2. In essence, MX with Java is quite simple, as every Java object is
   equipped with a lock.  By default, however, these locks are not
   used.  Instead, you need to use the keyword ~synchronized~ if you want
   threads to acquire the locks for MX.

The simplest way to enforce MX is to declare methods operating on
shared resources as ~synchronized~.
If a thread T1 wants to execute such a synchronized method on some object,
then thread T1 will automatically try to acquire the lock for that
object.  If that lock has been taken, say by thread T0, then T1 will
be blocked until T0 leaves the method and releases the lock.

Besides, you can also use other objects for synchronization if you
want to turn blocks of code into critical sections.  We will not use
this, however.

Finally, the Java monitor concept includes a mechanism for cooperation
of threads based on wait sets, which will be explained later.
#+END_NOTES

** [[#java-monitor-intro][Recall:]] ~synchronized~ Example
#+BEGIN_SRC java
public synchronized void sell() {
  if (seatsRemaining > 0) {
    dispenseTicket();
    seatsRemaining = seatsRemaining - 1;
  } else displaySorrySoldOut();
}
#+END_SRC
   #+ATTR_REVEAL: :frag (appear)
   - [[#jitt-synchronized-sell][As you observed above]],
     ~synchronized~ avoids races
     - Method executed under MX
     - Threads need to acquire lock on ~this~ object before executing
       method
   - Really, it is that simple!

* Cooperation with Monitors in Java

** General Idea
   :PROPERTIES:
   :CUSTOM_ID: cooperation-idea
   :END:
   #+INDEX: Monitor!Cooperation (MX II)
   - Threads may work with different roles on shared data structures
     - E.g., [[file:Operating-Systems-MX.org::#producer-consumer][producer/consumer problems seen earlier]]
   - Some may find that they cannot continue before others did their work
     - The former call ~wait()~ and hope for ~notify()~ by the latter
     - Cooperation (*orthogonal to and not necessary for MX!*)
       - General [[#monitor-origin][monitor concept]]
       - *Wait set* mentioned [[#java-monitors][above]] and explained
         subsequently

** ~wait()~ and ~notify()~ in Java
   :PROPERTIES:
   :CUSTOM_ID: wait-notify
   :reveal_extra_attr: data-audio-src="./audio/6-wait-notify-1.ogg"
   :END:
   #+INDEX: Monitor!wait() (MX II)
   #+INDEX: wait() (MX II)
   #+INDEX: Monitor!notify() (MX II)
   #+INDEX: notify() (MX II)
   #+ATTR_REVEAL: :frag (appear) :audio (./audio/6-wait-notify-2.ogg ./audio/6-wait-notify-3.ogg)
   - Waiting via blocking
     - ~wait()~: thread *unlocks* and *leaves* monitor, enters *wait set*
       - Thread enters
         [[file:Operating-Systems-Scheduling.org::#thread-states][state]]
         *blocked* (no busy waiting)
       - Called by thread that cannot continue (without work/help of another thread)
   - Notifications
     - ~notify()~
       - Remove one thread from *wait set* (if such a thread exists)
	 - Change state from blocked to runnable
       - Called by thread whose work may help another thread to continue
     - ~notifyAll()~
       - Remove all threads from *wait set*
	 - Only one can lock and enter the monitor, of course
	 - Only after the notifying thread has left the monitor, of course
	 - Overhead (may be avoidable with appropriate synchronization
           objects or with [[file:Operating-Systems-MX.org::#semaphore-example][semaphores as seen previously]])
#+BEGIN_NOTES
1. Cooperation between threads sharing resources can be
   managed with the methods wait() and notify() (or notifyAll()).
   A thread can only invoke these methods on an object if it has
   acquired the lock for that object, i.e., if it currently executes
   inside the object’s monitor.  So, usually, you see invocations of
   wait and notify in synchronized methods.

2. If a thread finds that it cannot make use of the shared resource in
   the resource’s current state, it can invoke wait() to release the
   lock on that resource and leave its monitor.  At that point in
   time, the thread’s state changes to blocked, and the thread is
   recorded in a special data structure associated with the object,
   called wait set.  In the wait set, Java keeps track of all threads
   that have invoked wait() on the object.  So, once a thread has
   executed wait(), the object’s lock is released, and other threads
   can acquire the object’s lock and modify the object’s state.

3. If a thread has modified the object’s state in such a way that
   there is reason to believe that waiting threads might now be able to
   continue, the thread invokes notify() on the object, which removes
   one thread from the wait set and makes it runnable.  When that
   runnable thread is scheduled for execution later on, it can again
   try to enter to monitor by locking the object; once the lock has
   been acquired, the thread resumes execution after the wait()
   method.
   The method notifyAll() is an alternative to notify() that removes
   all threads from the wait set, not just one.  You may want to think
   about advantages and disadvantage of notifying all waiting threads
   yourself.
#+END_NOTES

* BoundedBuffer in Java
** Bounded Buffers
   :PROPERTIES:
   :CUSTOM_ID: bounded-buffer
   :END:
   #+INDEX: Bounded buffer (MX II)
   - A *buffer* is a data structure to store items, requests,
     responses, etc.
     - Lots of buffer variants exist
     - A *bounded* buffer has a limited capacity
       - E.g., a [[basic:https://docs.oracle.com/javase/tutorial/java/nutsandbolts/arrays.html][Java array]]
         or any other data structure of limited capacity
   - As with any other data structure, MX is necessary when buffers
     are shared
     - Subsequently, two alternative buffer implementations with MX
       - Java’s monitor concept (with array as underlying, shared data structure)
       - Java Semaphore (with list as underlying, shared data structure)

** Sample ~synchronized~ Java Method
   :PROPERTIES:
   :CUSTOM_ID: java-bounded-buffer
   :END:
#+BEGIN_SRC java
// Based on Fig. 4.17 of [Hai17]
public synchronized void insert(Object o)
  throws InterruptedException
// Called by producer thread
{
  while(numOccupied == buffer.length)
      // block thread as buffer is full;
      // cooperation from consumer required to unblock
      wait();
  buffer[(firstOccupied + numOccupied) % buffer.length] = o;
  numOccupied++;
  // in case any retrieves are waiting for data, wake/unblock them
  notifyAll();
}
#+END_SRC
(Part of [[https://gitlab.com/oer/OS/-/blob/master/java/SynchronizedBoundedBuffer.java][SynchronizedBoundedBuffer.java]])

*** Comments on ~synchronized~
    - Previous method in larger program: [[https://gitlab.com/oer/OS/blob/master/java/BBTest.java][BBTest.java]]
      - ~SynchronizedBoundedBuffer~ as shared resource
      - Different threads
	([[https://gitlab.com/oer/OS/blob/master/java/Producer.java][Producer]]
	instances and [[https://gitlab.com/oer/OS/blob/master/java/Consumer.java][Consumer]] instances)
	call ~synchronized~ methods on that bounded buffer
	- Before methods are executed, lock of buffer needs to be acquired
	  - This enforces MX for methods ~insert()~ and ~retrieve()~
	- In methods, threads call ~wait()~ on buffer if unable to continue
	  - ~this~ object used implicitly as target of ~wait()~
	  - Thread enters wait set of buffer
	  - Until ~notifyAll()~ on same buffer
	- Note that thread classes contain neither
          ~synchronized~ nor ~wait/notify~

** Sample Semaphore Use in Java
   :PROPERTIES:
   :CUSTOM_ID: semaphore-example
   :END:
   #+INDEX: Semaphore!Example (MX II)

#+INCLUDE: "./java/SemaphoreBoundedBuffer.java" src java
*** Comments on Java Semaphore
    - Java provides [[https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/util/concurrent/Semaphore.html][~java.util.concurrent.Semaphore~]]
      - Implements [[file:Operating-Systems-MX.org::#semaphore-origin][semaphore concept]]
        - Constructor with integer argument to track resources
        - Methods ~acquire()~ and ~release()~
    - ~SemaphoreBoundedBuffer~ implements same interface as
      ~SynchronizedBoundedBuffer~
      - Use whichever you want with [[https://gitlab.com/oer/OS/blob/master/java/BBTest.java][BBTest.java]]
      - Bounded buffer uses three semaphores
        - One (initialized to 1) acting as mutex
          - Note ~acquire()~ and ~release()~ around buffer accesses for MX
        - Other two counting occupied and free places

* Conclusions
** Summary
   - Java objects can act as monitors
     - Keyword ~synchronized~
       - MX for CS (method/block of code)
	 - No flags, no explicit locks!
     - Cooperation via ~wait()~ and ~notify()~

#+INCLUDE: "backmatter.org"
