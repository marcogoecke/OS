BattleThreads
===


# Own ships

In this grid, place your team's ships.  Later, record shots by the other team.  Use "X" to indicate positions of your ships, and  "m" (miss), "h" (hit), or "d" (destroyed) for shots.

|    |  1 |  2 |  3 |  4 |  5 |  6 |  7 |  8 |
| -- | -- | -- | -- | -- | -- | -- | -- | -- |
| A  |    |    |    |    |    |    |    |    |
| B  |    |    |    |    |    |    |    |    |
| C  |    |    |    |    |    |    |    |    |
| D  |    |    |    |    |    |    |    |    |
| E  |    |    |    |    |    |    |    |    |
| F  |    |    |    |    |    |    |    |    |
| G  |    |    |    |    |    |    |    |    |
| H  |    |    |    |    |    |    |    |    |


# Their territory

Here, you record shots into your opponent's territory.  Use "m" (miss), "h" (hit), or "d" (destroyed).

|    |  1 |  2 |  3 |  4 |  5 |  6 |  7 |  8 |
| -- | -- | -- | -- | -- | -- | -- | -- | -- |
| A  |    |    |    |    |    |    |    |    |
| B  |    |    |    |    |    |    |    |    |
| C  |    |    |    |    |    |    |    |    |
| D  |    |    |    |    |    |    |    |    |
| E  |    |    |    |    |    |    |    |    |
| F  |    |    |    |    |    |    |    |    |
| G  |    |    |    |    |    |    |    |    |
| H  |    |    |    |    |    |    |    |    |
