# Local IspellDict: en
#+SPDX-FileCopyrightText: 2017-2021 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+STARTUP: showeverything
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="./reveal.js/dist/theme/index.css" />
#+TITLE: Index terms for OER course on Operating Systems
#+AUTHOR: Jens Lechtenbörger
#+OPTIONS: html-style:nil
#+OPTIONS: toc:nil num:nil

#+INCLUDE: "theindex.inc"
