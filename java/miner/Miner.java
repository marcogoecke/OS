// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2022 Jens Lechtenbörger

import java.util.concurrent.ThreadLocalRandom;
import java.security.MessageDigest;
import java.nio.charset.StandardCharsets;

/**
 * The class Miner serves as example for a CPU bound thread.
 *
 * A CPU bound thread mostly wants to compute, with little interaction
 * with the outside world.  Here, a <code>Miner</code> needs some
 * input string, which it retrieves from a bounded buffer.  Once the
 * Miner retrieved a string, it starts a computation that mimics the
 * proof-of-work in Bitcoin.  The details of this computation (which
 * is implemented in method <code>mine</code>) are not important for
 * class purposes, only that it takes long.  Beyond class purposes,
 * proof-of-work is an ecological disaster.  Its deployment in
 * real-world applications is unethical.
 */
public class Miner implements Runnable
{
    private BoundedBuffer buffer;
    private String name;

    private static final int difficulty = 15;

    public Miner(BoundedBuffer b, String n) {
        buffer = b;
        name = n;
    }

    public void run() {
        while (true) {
            try {
                String line = (String) buffer.retrieve();
                System.out.println(name + " retrieved: " + line);
                String hash = mine(line);
                System.out.println(name + " mined a line: " + hash);
            } catch(InterruptedException e) { }
        }
    }

    /**
     * Implement a proof-of-work mining process.
     *
     * Repeatedly hash the input message with a random nonce until the
     * resulting hash value satisfies a difficulty condition.  The
     * difficulty is defined by a number of leading zeros that the hash
     * value must have.
     *
     * As stated above: Proof-of-work is an ecological disaster, its
     * use in real-world applications is unethical.
     */
    private static String mine(final String message) {
        String hash;
        int random;
        do {
            random = ThreadLocalRandom.current().nextInt();
            hash = hash("" + random + message);
        } while(!hash.substring(0, difficulty).equals("0".repeat(difficulty)));
        return "nonce:" + random + "##hash:" + hash;
    }

    /**
     * Hash a string with SHA256 and return result as hex string.
     *
     * Code copied from StackOverflow:
     * https://stackoverflow.com/questions/5531455/how-to-hash-some-string-with-sha256-in-java
     */
    private static String hash(final String message) {
        try {
            final MessageDigest digest = MessageDigest.getInstance("SHA-256");
            final byte[] hash = digest.digest(message.getBytes(StandardCharsets.UTF_8));
            final StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < hash.length; i++) {
                final String hex = Integer.toHexString(0xff & hash[i]);
                if(hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch(Exception ex) { throw new RuntimeException(ex); }
    }
}
