# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config-course.org"
#+MACRO: jittquiz [[https://sso.uni-muenster.de/LearnWeb/learnweb2/course/view.php?id=60751#section-10][Learnweb]]

#+TITLE: OS07: MX Challenges
#+SUBTITLE: Based on Chapter 4 of cite:Hai19
#+KEYWORDS: mutual exclusion, priority inversion, deadlock, convoy, starvation
#+DESCRIPTION: Third of three presentations on mutual exclusion in the context of operating systems.  Discusses challenges (priority inversion, deadlock, convoy, starvation) and counter measures in view of mutual exclusion

* Introduction

# This is the eighth presentation for this course.
#+CALL: generate-plan(number=8)

** Today’s Core Question
   - Am I fine if I lock all shared resources before use?

   #+ATTR_REVEAL: :frag (appear)
   (Short answer: No.  Issues such as priority inversion, deadlocks,
   starvation may arise.)

** Learning Objectives
   - Explain priority inversion and counter measures
   - Explain and apply deadlock prevention and detection
   - Explain convoys and starvation as MX challenges

** Previously on OS …
    - Mutexes may be based either on busy waiting ([[file:Operating-Systems-MX.org::#spinlocks][spinlocks]])
      or on blocking (e.g., [[file:Operating-Systems-MX.org::#locking][lock, mutex]],
      [[file:Operating-Systems-MX.org::#semaphore-origin][semaphore]], [[file:Operating-Systems-MX-Java.org::#monitor-origin][monitor]])
    - Threads may have different *priorities*
      - Lower priority threads are *preempted* for those with higher
	priority (e.g., with [[file:Operating-Systems-Scheduling.org::#roundrobin][round robin scheduling]]),
	which may lead to [[file:Operating-Systems-Scheduling.org::#priority-starvation][starvation]]

*** Threads, again
#+REVEAL_HTML: <script data-quiz="quizThreadMisc" src="./quizzes/thread-misc.js"></script>

** Different Learning Styles
    - In previous years, some students reported that Section 4.8.1 (pp. 135
      -- 137) of cite:Hai19 on Priority Inversion is quite easy to
      understand, while they perceived
      [[#priority-inversion][that section in this presentation]]
      to be confusing.
    - Note that cite:Hai19 discusses Priority Inversion resulting from
      locks/mutexes with blocking, while the slides also contain a
      variant with spinlocks.

** Table of Contents
   :PROPERTIES:
   :UNNUMBERED: notoc
   :HTML_HEADLINE_CLASS: no-toc-progress
   :END:
#+REVEAL_TOC: headlines 1

* Priority Inversion
  :PROPERTIES:
  :CUSTOM_ID: priority-inversion
  :reveal_extra_attr: data-audio-src="./audio/7-pi.ogg"
  :END:
  #+INDEX: Priority inversion!General remarks (MX III)
#+BEGIN_NOTES
In general, if threads with different priorities exist, the OS should
run those with high priority in preference to those with lower
priority.

The technical term “priority inversion” denotes phenomena, where
low-priority threads hinder the progress of high-priority threads,
which intuitively should not happen.
The next slides demonstrate such phenomena, first a weaker variant
where MX is enforced with spinlocks, then the more usual variant with
MX based on blocking.
#+END_NOTES

#+MACRO: T0   [[color:darkgreen][T_0]]
#+MACRO: TM   [[color:darkorange][T_M]]
#+MACRO: T1   [[color:red][T_1]]

** Priority Inversion with Spinlocks
   :PROPERTIES:
   :CUSTOM_ID: pi-spinlocks
   :END:
   #+INDEX: Priority inversion!With spinlocks (MX III)
   - Example; single CPU core (visualization on next slide)
     #+ATTR_REVEAL: :frag (appear)
     1. Thread {{{T0}}} with low priority enters CS
     2. {{{T0}}} preempted by OS for {{{T1}}} with high priority
        - E.g., an [[color:red][important event]] occurs, to be handled by {{{T1}}}
        - Note that {{{T0}}} is still inside CS, holds lock
     3. {{{T1}}} tries to enter same CS and *spins* on lock held by {{{T0}}}

   #+ATTR_REVEAL: :frag appear
   - This is a variant of *priority inversion*
     - High-priority thread {{{T1}}} cannot continue due to actions by
       low-priority thread
       - If just one CPU core exists: Low-priority thread {{{T0}}}
         cannot continue
         - As CPU occupied by {{{T1}}}
         - Deadlock (discussed [[#deadlock][subsequently]])
       - If multiple cores exist: Low-priority thread {{{T0}}} runs
         although thread with higher priority does not make progress

*** Single Core
    :PROPERTIES:
    :CUSTOM_ID: pi-single-core
    :reveal_extra_attr: data-audio-src="./audio/7-pi-spinning.ogg"
    :END:
    {{{revealimg("./figures/OS/7-PI-spin-1.meta",nil,"60rh",nil,t)}}}

    See [[#pi-spinlocks][previous slide]] or notes for explanations
    #+begin_notes
Green thread T0 executes on the single CPU core where is enters a CS,
holding a lock, until the red thread T1 with higher priority arrives.
At that point in time, T0 is preempted (still inside the CS, holding a
lock), and T1 runs, wanting to enter the CS.  T1 now spins on the lock
forever.
    #+end_notes

*** Two Cores
    :PROPERTIES:
    :CUSTOM_ID: pi-two-cores
    :reveal_extra_attr: data-audio-src="./audio/7-pi-two-cores.ogg"
    :END:
    {{{revealimg("./figures/OS/7-PI-spin-2.meta",nil,"60rh",nil,t)}}}

    See [[#pi-spinlocks][earlier slide]] or notes for explanations
    #+begin_notes
This time, two CPU cores are available.  As before, green thread T0
executes and is inside a CS, holding a lock.  When red thread T1 with
higher priority arrives, it can be dispatched for execution on the
second core.  So, T0 and T1 run in parallel.  T1 tries to enter the
CS, but needs to spin on the lock held by T0 for some time.  As T1 has
higher priority than T0, T1 should intuitively not be delayed by T0.
Thus, we say that this is a variant of priority inversion.

When T0 leaves the CS and releases the lock, T1 is able to enter the
CS.
    #+end_notes

** Priority Inversion with Blocking
   :PROPERTIES:
   :CUSTOM_ID: pi-blocking
   :END:
   #+INDEX: Priority inversion!With blocking (MX III)
   - (Visualization on next slide)
   - {{{T0}}} with [[color:darkgreen][low]],
     {{{TM}}} with [[color:darkorange][medium]],
     {{{T1}}} with [[color:red][high]] priority
     #+ATTR_REVEAL: :frag (appear)
     1. {{{T0}}} in CS
     2. An [[color:red][important event]] occurs, OS preempts {{{T0}}} for {{{T1}}}
       - {{{T1}}} attempts entry into same CS, {{{T1}}} gets blocked
       - {{{T0}}} could continue if no higher priority thread existed
     3. Another, [[color:darkorange][less important]], event occurs,
        to be handled by {{{TM}}}
       - Based on priority, OS favors {{{TM}}} over {{{T0}}}
       - {{{TM}}} runs instead of more highly prioritized {{{T1}}} → *Priority
         inversion*
         - ({{{TM}}} does unrelated work, without need to enter the CS)
       - {{{T0}}} cannot leave CS as long as {{{TM}}} exists
   #+ATTR_REVEAL: :frag appear
   - With long running or many threads of [[color:darkorange][medium]] priority,
     {{{T1}}} (and [[color:red][important event]]) need to wait for a long time

*** Blocking CS
    {{{revealimg("./figures/OS/7-PI-blocking.meta",nil,"60rh",nil,t)}}}

    See [[#pi-blocking][previous slide]] for explanations

** Priority Inversion Example
   :PROPERTIES:
   :CUSTOM_ID: priority-inversion-example
   :END:
   #+INDEX: Priority inversion!Pathfinder example (MX III)
   - Mars Pathfinder, 1997;
     [[beyond:https://en.wikipedia.org/wiki/Mars_Pathfinder][Wikipedia]] offers details
     - Robotic spacecraft named Pathfinder
       {{{reveallicense("./figures/photos/262px-Sojourner_on_Mars_PIA01122.meta","30rh")}}}
       - With rover named Sojourner (shown to right)
     - A “low-cost” mission at $280 million
   - Bug (priority inversion) caused repeated resets
     - “found in preflight testing but was deemed a *glitch* and therefore
       given a low priority as it only occurred in certain *unanticipated*
       heavy-load conditions”
   - Priority inversion had been known for a long time
     - E.g.: cite:LR80

** Priority Inversion Solutions
   :PROPERTIES:
   :CUSTOM_ID: priority-inheritance
   :reveal_extra_attr: data-audio-src="./audio/7-priority-inheritance-1.ogg"
   :END:
   #+INDEX: Priority inversion!Solutions (MX III)
   #+INDEX: Priority inheritance (MX III)
   #+INDEX: Priority ceiling (MX III)
   - *Priority Inheritance (PI)*
     - Thread of low priority inherits priority of waiting thread
       - E.g., [[file:Operating-Systems-MX.org::#pi-futex][PI-futex in Linux]]
       - E.g., [[beyond:https://web.archive.org/web/20190413060936/https://www.microsoft.com/en-us/research/people/mbj/#!just-for-fun][remote update for Mars Pathfinder]]
	 - Mutex of Pathfinder OS had flag to activate PI
	 - Initially, PI was off …
   #+ATTR_REVEAL: :frag appear :audio ./audio/7-priority-ceiling.ogg
   - *Priority Ceiling (PC)*
     - Every resource has priority (new concept; so far only threads
       had priorities)
       - (Highest priority that “normal” threads can have) + 1
     - Accessing thread runs with that priority
   #+ATTR_REVEAL: :frag appear :audio ./audio/7-priority-inheritance-2.ogg
   - In both cases: Restore old priority after access
#+BEGIN_NOTES
An intuitive explanation of Priority Inheritance is that if an
important task, i.e., a thread with high priority, needs to wait for
“something else”, then this “something else” immediately gains in
importance, as the success of the important task depends on it.

Thus, with Priority Inheritance a thread of low priority holding some
lock, mutex, semaphore, or monitor, for which a high priority threads waits,
inherits the priority of the high priority waiting thread.

If you think again about the negative effects of medium priority
threads for priority inversion, those negative effects do no longer
occur, as the thread with inherited priority is now scheduled before
medium priority threads.  Thus, it finishes fast, allowing the high
priority thread to continue quickly.

For Priority Ceiling, each resource is assigned a priority.  The
priority of a thread accessing that resource is then increased to the
resource’s priority.  Usually, the priority for resources is set to be
slightly higher than that of ordinary threads.  Thus, for the duration
of resource accesses, threads run with highest priority and finish
quickly.

In both cases, Priority Inheritance and Priority Ceiling, priorities
are restored after resource access.

To conclude, you should think twice whether you want to create threads
with different priorities that share resources.  If yes, priority
inversion may happen.  Then, you need to check the documentation for
whatever MX mechanism you are about to apply whether it supports PI or
PC.  If neither is documented, do not use that mechanism.
#+END_NOTES

** JiTT and Exercise Task
   :PROPERTIES:
   :CUSTOM_ID: exercise-7-1
   :reveal_data_state: no-toc-progress
   :reveal_extra_attr: class="jitt"
   :END:
   - This task is a *variant* of Exercise 4.10 of cite:Hai19.
     Solve part (1) as self-study in
     {{{jittquiz}}}, part (2) as exercise task.
     - Suppose a computer with only one processor runs a program that
       creates three threads, which are assigned high,
       medium, and low fixed priorities.  (Assume that no other threads
       are competing for the same processor.)  The threads of high and
       medium priority are currently blocked, waiting for different
       events, while the thread with low priority is runnable.  Some
       threads share a single mutex (to protect shared resources that
       are not shown).
       Pseudocode for each of the threads is shown subsequently.
       1. Suppose that the mutex does not provide priority inheritance.
          How soon would you expect the program to terminate? Why?
       2. Suppose that the mutex provides priority inheritance.  How soon
          would you expect the program to terminate? Why?
     #+begin_src
High-priority thread:
  “initially blocked; unblocked to handle event after 200 milliseconds”
  perform lock() on mutex
  run for 2 milliseconds on CPU
  perform unlock() on mutex
  terminate execution of the whole program

Medium-priority thread:
  “initially blocked; unblocked to handle event after 200 milliseconds”
  run for 500 milliseconds on CPU

Low-priority thread:
  “initially runnable”
  perform lock() on mutex
  perform I/O operation which leads to blocking for 600 milliseconds
  run for 3 milliseconds on CPU
  perform unlock() on mutex
     #+end_src


* Deadlocks

** Deadlock
   :PROPERTIES:
   :CUSTOM_ID: deadlock
   :reveal_extra_attr: data-audio-src="./audio/7-deadlocks.ogg"
   :END:
   #+INDEX: Deadlock!Deadlock intuition (MX III)
   - Permanent blocking of thread set
     {{{reveallicense("./figures/OS/Gridlock.meta","40rh")}}}
     - Reason
       - *Cyclic waiting* for resources/locks/messages of other threads
       - ([[#deadlock-def][Formal definition on later slide]])
   - No generally accepted solution
     - Deadlocks can be perceived as programming bugs
       - Dealing with deadlocks causes overhead
	 - Acceptable to deal with (hopefully rare) bugs?
     - Solutions depend on
       - Properties of resources (e.g., [[#deadlock-prevention][linearly ordered ones]])
       - Properties of threads (transactions?)
   #+begin_notes
A /deadlock/ is a programming bug, which leads to multiple threads
being stuck: In essence, the threads mutually wait for something from
other threads which never arrives.

To get a feeling for deadlocks, note that some traffic situations can
be interpreted as deadlocks.  First, the image here shows a traffic
situation where no car can move because other cars (in particular, the
red ones) block required street segments.  In OS terms, the cars can
be interpreted as threads, which are stuck, while street segments
represent shared resources under MX that are exclusively owned by some
threads while others also need them.  This is an instance of cyclic
waiting.

As a different example, consider
[[https://en.wikipedia.org/wiki/Priority_to_the_right][priority to the right]]
and a street crossing where four cars arrive from all four
directions.  Under “priority to the right”, each driver needs to wait
for another car to move first.  Thus, neither can move, all are stuck.

In programming, we aim to avoid problematic algorithms or rules such a
“priority to the right” so that deadlocks do not occur.  However,
there is no generally accepted solution, and you need to be
particularly careful when using MX mechanisms.

As you will see, OSs typically ignore deadlocks, which is justified by
the reasoning that /programmers/ should avoid this type of /bug/; thus,
there is no need to add additional complexity and overhead to the OS.
Moreover, solutions also depend on the type of resources, e.g., you
will see a strategy for linearly ordered resources later on.

As a side note, database systems may involve deadlock detection for
/transactions/, which can be aborted to undo their effects, while this
is less simple for threads in OSs.  Thus, thread properties also play
a role in deadlock considerations.
   #+end_notes

** Deadlock Example
   :PROPERTIES:
   :CUSTOM_ID: deadlock-ex
   :END:
   #+INDEX: Deadlock!Deadlock example with bank accounts (MX III)
   - Money transfers between bank accounts
     - Transfer from ~myAccount~ to ~yourAccount~ by thread 1; transfer in
       other direction by thread 2
   #+ATTR_REVEAL: :frag appear
   - *Race conditions* on account balances
   - Need *mutex* per account
     - Lock both accounts involved in transfer. What order?
   #+ATTR_REVEAL: :frag appear
   - “Natural” lock order: First, lock source account; then, lock
     destination account
     - Thread 1 locks ~myAccount~, while thread 2 locks ~yourAccount~
       - Each thread gets blocked once it attempts to acquire the
         second lock
	 - Neither can continue
       - *Deadlock*

** Defining Conditions for Deadlocks
   :PROPERTIES:
   :CUSTOM_ID: deadlock-def
   :END:
   #+INDEX: Deadlock!Deadlock definition (MX III)
   Deadlock if and only if (1) -- (4) hold cite:CES71:

   1. *Mutual exclusion*
      - Exclusive resource usage
   2. *Hold and wait*
      - Threads hold some resources while waiting for others
   3. *No preemption*
      - OS does not forcibly remove allocated resources
   4. *Circular wait*
      - Circular chain of threads such that each thread holds
        resources that are requested by next thread in chain

** Resource Allocation Graphs
   :PROPERTIES:
   :CUSTOM_ID: resource-allocation-graphs
   :END:
   #+INDEX: Deadlock!Resource allocation graph (MX III)
   #+INDEX: Resource allocation graph (MX III)
   - Representation and visualization of resource allocation as
     directed graph
     - (Necessary prior knowledge:
       [[basic:https://en.wikipedia.org/wiki/Directed_graph][directed graphs]]
       and [[basic:https://en.wikipedia.org/wiki/Cycle_(graph_theory)][cycles]]
       in such graphs)
     - Nodes
       - Threads (squares on next slide)
       - Resources (circles on next slide)
     - Edges
       - From thread T to resource R if T is waiting for R
       - From resource R to thread T if R is allocated to T
     - [[#resource-allocation-graph-example][Example on next slide]]
   - *Fact*: System in deadlock if and only if graph contains cycle

** Resource Allocation Graph Example
   :PROPERTIES:
   :CUSTOM_ID: resource-allocation-graph-example
   :END:
   Visualization of deadlock: cyclic resource allocation graph
   for [[#deadlock-ex][previous example]]

   {{{revealimg("./figures/OS/hail_f0422.pdf.meta",t,"40rh")}}}

   #+ATTR_HTML: :class smaller
   (Note: Choice of shapes is arbitrary; just for visualization purposes)

* Deadlock Strategies

** Deadlock Strategies
   :PROPERTIES:
   :CUSTOM_ID: deadlock-strategies
   :END:
   #+INDEX: Deadlock!Deadlock strategies (MX III)
   - (Ostrich algorithm)
   - Deadlock Prevention
   - Deadlock Avoidance
   - Deadlock Detection

   These strategies are covered in subsequent slides.

** Ostrich “Algorithm”
   :PROPERTIES:
   :CUSTOM_ID: ostrich
   :END:
   #+INDEX: Ostrich algorithm (MX III)
   #+BEGIN_leftcol
   - A joke about missing deadlock handling
     - “Implemented” in most systems
       - Pretend nothing special is happening
       - (E.g., Java VMs act like ostriches)
     - Reasoning
       - Proper deadlock handling is complex
       - Deadlocks are rare, result from buggy programs
   #+END_leftcol

   #+BEGIN_rightcol
   #+HTML: <div class="figure randomPic"><!-- { "imgalt": "Ostrich visualization", "imgcaption": "<p></p><p>Drawing by {name}; released into Public Domain; randomly selected from <a href='https://gitlab.com/oer/OS/tree/master/img/ostriches'>excellent drawings</a> (refresh for others)</p>", "choices": [ {"name": "Thomas Ackermann", "path": "./img/ostriches/ostrich-ackermann.png"}, {"name": "Fabian Buschmann", "path": "./img/ostriches/ostrich-buschmann.png"}, {"name": "Philipp Käfer", "path": "./img/ostriches/ostrich-kaefer.png"}, {"name": "Adrian Lison", "path": "./img/ostriches/ostrich-lison.png"}, {"name": "Felix Murrenhoff", "path": "./img/ostriches/ostrich-murrenhoff.png"}]} --></div>

   #+ATTR_HTML: :class smaller
   (Refresh HTML presentation for other drawings)
   #+END_rightcol

#+BEGIN_EXPORT latex
\begin{figure}[htp]
\centering
\includegraphics[width=.9\linewidth]{img/ostriches/ostrich-lison.png}
\caption{Drawing created by Adrian Lison for bonus task in summer term 2017; released into Public Domain; \href{https://gitlab.com/oer/OS/tree/master/img/ostriches}{other excellent drawings}.}
\end{figure}
#+END_EXPORT

** Deadlock Prevention
   :PROPERTIES:
   :CUSTOM_ID: deadlock-prevention
   :reveal_extra_attr: data-audio-src="./audio/7-prevention.ogg"
   :END:
   #+INDEX: Deadlock!Deadlock prevention strategies (MX III)
   #+INDEX: Deadlock!Linear resource ordering!Definition (MX III)
   #+INDEX: Linear resource ordering!Definition (MX III)
   - Prevent a [[#deadlock-def][defining condition]] for deadlocks
     from becoming true
   - Practical options
     #+ATTR_REVEAL: :frag appear
     - Prevent condition (2), “hold and wait”: Request all necessary
       resources at once
       - Only possible in special cases, e.g., [[basic:https://en.wikipedia.org/wiki/Two-phase_locking#Conservative_two-phase_locking][conservative/static 2PL]] in DBMS
       - Threads either have no incoming or no outgoing edges in
         resource allocation graph → Cycles cannot occur
     #+ATTR_REVEAL: :frag appear
     - Prevent condition (4), “circular wait”: Number resources,
       request resources according to *linear resource ordering*
       - Consider resources R_h and R_k with h < k
         - Threads that need both resources must lock R_h first
         - Threads that already requested R_k do not request R_h afterwards
       - Requests for resources in ascending order → Cycles cannot occur
#+BEGIN_NOTES
A strategy for deadlocks is called prevention strategy if it prevents
deadlocks from happening by making sure that one of the four defining
deadlock conditions can never become true.  Although there are
four conditions, only two of them are used for practical purposes, and
they are explained in the subsequent bullet points.
Please think about those bullet points on your own.
#+END_NOTES

*** Linear Resource Ordering Example
    :PROPERTIES:
    :CUSTOM_ID: linear-ordering-example
    :END:
    #+INDEX: Deadlock!Linear resource ordering!Example (MX III)
    #+INDEX: Linear resource ordering!Example (MX III)
    - Money transfers between bank accounts revisited
    - Locks acquired in *order of account numbers*
      - A programming contract, not known by OS
      - Suppose ~myAccount~ has number 42, ~yourAccount~ is 4711
	- Both threads try to lock ~myAccount~ first (as 42 < 4711)
	  - Only one succeeds, can also lock ~yourAccount~
	  - The other thread gets blocked
      - *No deadlock*
    - (See Fig 4.21 in cite:Hai19 for an example of linear ordering in
      the context of the Linux scheduler)

** Deadlock Avoidance
   :PROPERTIES:
   :CUSTOM_ID: deadlock-avoidance
   :reveal_extra_attr: data-audio-src="./audio/7-avoidance-1.ogg"
   :END:
   #+INDEX: Deadlock!Deadlock avoidance strategies (MX III)
   - (See [[https://ell.stackexchange.com/questions/52710/the-difference-between-prevent-and-avoid][stackexchange for difference between prevent and avoid]])
#+ATTR_REVEAL: :frag (appear) :audio (./audio/7-avoidance-2.ogg ./audio/7-avoidance-3.ogg)
   - Dynamic decision whether allocation may lead to deadlock
     - If a deadlock cannot be ruled out easily: Do not perform that
       allocation but *block* the requesting thread (or return error
       code or raise exception)
     - Consequently, deadlocks do never occur; they are *avoided*
   - Classical technique
     - [[beyond:https://en.wikipedia.org/wiki/Banker's_algorithm][Banker’s algorithm]] by Dijkstra
       - Deny incremental allocation if “unsafe” state would arise
       - Not used in practice
	 - Resources and threads’ requirements need to be declared ahead of
           time
#+BEGIN_NOTES
A strategy for deadlocks is called avoidance strategy if it avoids
deadlocks.  Personally, I don’t see much difference between the words
“prevent” and “avoid”, but this terminology is accepted in the
literature on deadlocks.

Avoidance does not rule out any specific of the four defining deadlock
conditions, but it still makes sure that deadlocks will not happen.
The typical approach is to analyze resource requests by threads.  If
some deadlock avoidance algorithm is able to rule out a deadlock for
the resulting state, the request will be granted.  If the algorithm is
not able to rule out deadlocks, the request will not be granted.  Note
that such algorithms generally err on the safe side.  Thus, some
requests might not be granted although they would not cause any
deadlock; the OS might be unable to detect this, though.

A famous deadlock avoidance technique is Dijkstra’s banker’s
algorithm, which has quite restrictive preconditions and is therefore not
used in practice.
#+END_NOTES

** Deadlock Detection
   :PROPERTIES:
   :CUSTOM_ID: deadlock-detection
   :reveal_extra_attr: data-audio-src="./audio/7-detection-1.ogg"
   :END:
   #+INDEX: Deadlock!Deadlock detection (MX III)
   - Idea
     - Let deadlocks happen
     - Detect deadlocks, e.g., via cycle-check on resource allocation graph
       - Periodically or
       - After “unreasonably long” waiting time for lock or
       - Immediately when thread tries to acquire a locked mutex
     - Resolve deadlocks: typically, terminate some thread(s)
   #+ATTR_REVEAL: :frag appear :audio ./audio/7-detection-2.ogg
   - Prerequisite to build graph
     - Mutex records by which thread it is locked (if any)
     - OS records for what mutex a thread is waiting
#+BEGIN_NOTES
The final strategy for dealing with deadlocks is deadlock detection.
Here, the system does not take special precautions to avoid or prevent
deadlocks but lets them happen.  To deal with deadlocks, they are
detected, for example based on cycle checks on resource allocation
graphs, and then resolved.  Detection may take place periodically or after
waiting times or even immediately upon resource requests; the latter
actually prevents cyclic wait conditions, moving from deadlock
detection to deadlock prevention.
To resolve deadlocks, the OS typically terminates some threads until
no cycle exists any longer, and various strategies exist to select
victim threads.

Clearly, the OS needs to build suitable data structures for deadlock
detection, in case of resource allocation graphs, each mutex can
easily record by which thread it is locked, while the OS also keeps
track of what threads are waiting for what mutexes.
#+END_NOTES

* Further Challenges
** Convoy Problem
   :PROPERTIES:
   :CUSTOM_ID: convoy
   :reveal_extra_attr: data-audio-src="./audio/7-convoy.ogg"
   :END:
   #+INDEX: Convoy problem (MX III)
   - Suppose a central shared resource R exists
     - *Frequently accessed* by lots of threads, protected by *mutex M*
   - Preemption of thread T1 holding that mutex is likely
     - Other threads wind up in wait queue of mutex, the *convoy*
       - Thread switches *without much progress*
       - (See cite:BGM+79 for origin of “convoy” in context of
         database transactions)
   - Suppose T1 continues
     - T1 releases lock, which is reassigned to T2
     - During its time slice, T1 wants R again, but M is now held by T2
       - T1 gets blocked *without much progress*
   - The same happens to the other threads
     - The convoy *persists* for a long time
   #+begin_notes
The previous explanations of ~unlock()~
([[file:Operating-Systems-MX.org::#locks-mutexes][here]] and
[[file:Operating-Systems-MX.org::#mutex-queue][there]])
hinted at two alternatives to reassign unlocked locks.

The bullet points here argue that immediate reassignment of locks is
bad for frequently used resources: Here, lots of threads—among them T1
and T2—need the same shared resource, say R.  T1 currently holds the
mutex M that is used for MX on R, and T2 is blocked, waiting for M to
be unlocked.  If (a) T1 unlocks M and (b) the OS immediately assigns
the mutex to T2 (making T2 runnable but without any scheduling
decision yet, so T1 continues to run), then T1 cannot access R again
during its time slice, but will be blocked when trying to lock M.

If R is accessed frequently, threads will not be able to use much of
their time slices, leading to frequent context switches without much
progress in individual threads.  The threads blocked on M are
collected in a queue, which is called *convoy*.

A simple fix is explained on the next slide: When M is unlocked
by T1, the OS (a) changes the states of all threads that are blocked
on M (including T2) to runnable, but (b) does not reassign the mutex.
Thus, when T1 needs R again during its time slice, it can simply lock M
and proceed without problems.
   #+end_notes

*** A Convoy Solution
    :PROPERTIES:
    :CUSTOM_ID: convoy-solution
    :END:
    - Change [[file:Operating-Systems-MX.org::#mutex-queue][mutex]] behavior
      - Proposed in cite:BGM+79
      - Do no immediately reassign mutex upon ~unlock()~
      - Instead, make *all* waiting threads runnable
	- Without reassigning mutex
      - (In addition, for performance reasons in case of failing
        locking attempt cite:BGM+79 suggests “to spin for a few
        instructions in the hope that the lock will become free”)
    - Effect: T1 can ~lock()~ M repeatedly during its time slice

** Starvation
   :PROPERTIES:
   :CUSTOM_ID: starvation
   :reveal_extra_attr: data-audio-src="./audio/7-starvation.ogg"
   :END:
   #+INDEX: Starvation!Definition (MX III)
   - A thread *starves* if its resource requests are repeatedly denied
   - Examples in previous presentations
     - [[file:Operating-Systems-Interrupts.org::#starvation][Interrupt livelock]]
     - [[file:Operating-Systems-Scheduling.org::#priority-starvation][Thread with low priority in presence of high priority threads]]
     - [[file:Operating-Systems-MX.org::#mx-challenges][Thread which cannot enter CS]]
       - Famous illustration: Dining philosophers (next slide)
       - No simple solutions
   #+begin_notes
The term *starvation* occurred on several occasions already, where
threads could not continue their execution as expected but were
preempted or blocked frequently or for prolonged periods of time.
When locking is involved, avoidance of starvation is a hard problem
without simple solutions as illustrated next.
   #+end_notes

*** Dining Philosophers
    :PROPERTIES:
    :CUSTOM_ID: dining-philosophers
   :reveal_extra_attr: data-audio-src="./audio/7-philosophers.ogg"
    :END:
    #+INDEX: Starvation!Dining philosphers (MX III)
    - MX problem proposed by Dijkstra
    - Philosophers sit in circle; *eat* and think repeatedly
      - Two *forks* required for eating
	- *MX* for forks

   {{{revealimg("./figures/OS/hail_f0420.60.meta","Dining Philosophers","40rh")}}}
   #+begin_notes
A famous illustration of starvation, which alludes to the literal
meaning of the word, goes back to Dijkstra.  Here, philosophers need
forks to eat, and forks are protected by some MX mechanism.  If the
underlying algorithm to protect and reassign forks does not prevent
starvation, one or more philosophers may die from hunger as they do
not receive forks frequently enough.

Lots of textbooks on OS include algorithms for the dining philosophers
to explain MX, deadlocks, and starvation.  Inspired by an exercise in
cite:Sta01, the following slide shows a sequence of events that can
happen for the deadlock-free algorithm presented in cite:Tan01,
leading to starvation of a philosopher.

Apparently, avoidance of starvation is no simple task.
   #+end_notes

*** Starving Philosophers
    - Starvation of P0
      {{{reveallicense("./figures/OS/hail_f0420.60.meta","40rh")}}}
      #+ATTR_REVEAL: :frag (appear)
      - P1 and P3 or P2 and P4 eat in parallel
      - Then they wake the other pair
	- P1 wakes P2; P3 wakes P4
	- P2 wakes P1; P4 wakes P3
      - Iterate

* Conclusions
** Summary
   - MX to avoid race conditions
   - Challenges
     - Priority inversion
     - Deadlocks
     - Convoys
     - Starvation

#+INCLUDE: "backmatter.org"
