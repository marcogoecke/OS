# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config-course.org"
#+MACRO: jittquiz [[https://sso.uni-muenster.de/LearnWeb/learnweb2/course/view.php?id=60751#section-8][Learnweb]]

#+TITLE: OS03: Threads
#+SUBTITLE: Based on Chapter 2 of cite:Hai19
#+KEYWORDS: operating system, thread, process, context switch, concurrency, multitasking
#+DESCRIPTION: Discussion of thread concept for operating systems

* Introduction

# This is the fourth presentation for this course.
#+CALL: generate-plan(number=4)

** Today’s Core Questions
   - What exactly are threads?
     - Why and for what are they used?
     - How can I inspect them?
     - How are they created in Java?
     - What impact does blocking or non-blocking I/O have on the use
       of threads?
     - How does switching between threads work?

** Learning Objectives
   - Explain thread concept, thread switching, and multitasking
     - Including states (after upcoming presentation)
     - Explain distinctions between threads and processes
     - Explain advantages of a multithreaded organization in
       structuring applications and in performance
   - Inspect threads on your system
   - Create threads in Java
   - Discuss differences between and use cases for blocking and
     non-blocking I/O

** Retrieval practice

*** Informatik 1
    :PROPERTIES:
    :CUSTOM_ID: cs101
    :END:
    What are *interfaces* and *classes* in Java, what is
    “[[https://docs.oracle.com/javase/tutorial/java/javaOO/thiskey.html][this]]”?

    If you are not certain, consult a textbook; these self-check
    questions and preceding tutorials may help:
    - https://docs.oracle.com/javase/tutorial/java/concepts/QandE/questions.html
    - https://docs.oracle.com/javase/tutorial/java/IandI/QandE/interfaces-questions.html

*** Recall: Stack
    :PROPERTIES:
    :CUSTOM_ID: stack
    :END:
    #+INDEX: Stack!Definition (Threads)
    - Stack = Classical data structure (abstract data type)
      - LIFO (last in, first out) principle
      - See Appendix A in cite:Hai19 if necessary
    - Two elementary operations
      - ~Stack.Push(o)~: place object ~o~ on top of ~Stack~
      - ~Stack.Pop()~: remove object from top of ~Stack~ and return it
    - Supported in machine language of most processors (not in Hack,
      though)
      - Typically (e.g., x86), stack grows towards smaller addresses
        - Next object pushed gets smaller address than previous one
        - (Differently from stack of VM for Hack platform)

*** Drawing on Stack
    :PROPERTIES:
    :CUSTOM_ID: drawing-stack
    :END:
    #+INDEX: Stack!Drawing (Threads)
    {{{revealimg("./figures/external-non-free/jvns.ca/stack.meta",t,"60rh")}}}


*** Previously on OS …
    #+ATTR_REVEAL: :frag (appear)
    - What is a thread?
      {{{revealimg("./figures/external-non-free/jvns.ca/threads.meta",t,"40rh","figure fragment appear")}}}
    - What are [[file:Operating-Systems-Introduction.org::#multitasking][multitasking and scheduling]]?

*** Recall: Blocking vs Non-Blocking I/O
    :PROPERTIES:
    :CUSTOM_ID: blocking-vs-non-blocking
    :END:
    #+INDEX: I/O!Blocking I/O (Threads)
    #+INDEX: I/O!Non-blocking I/O (Threads)
    - For
      [[file:Operating-Systems-Interrupts.org::#non-blocking][blocking as well as non-blocking I/O]],
      thread invokes system call
      - OS is responsible for I/O
    - *Blocking* I/O: OS initiates I/O and schedules *different* thread for
      execution
      - Calling thread is *blocked* for duration of I/O
      - After I/O is finished, OS un-blocks calling thread
        - Un-blocked thread to be scheduled later on, with result of I/O system call
    - *Non-blocking* I/O: OS initiates I/O and returns (incomplete) result to
      calling thread

** Table of Contents
   :PROPERTIES:
   :UNNUMBERED: notoc
   :HTML_HEADLINE_CLASS: no-toc-progress
   :END:
#+REVEAL_TOC: headlines 1

* Threads
** Threads and Programs
   :PROPERTIES:
   :CUSTOM_ID: thread-definition
   :END:
   #+INDEX: Thread!Definition (Threads)
   - Program vs thread
     - Program contains instructions to be executed on CPU
     - OS [[file:Operating-Systems-Introduction.org::#multitasking][schedules]]
       execution of programs
       - By default, program execution starts with one *thread*
	 - *Thread* = unit of OS scheduling = independent sequence of
           computational steps
       - Programmer determines how many threads are created
	 - (OS provides [[file:Operating-Systems-Introduction.org::#system-calls][system calls]]
           for thread management, [[#java-threads][Java an API]])
     - Simple programs are single-threaded
     - More complex programs can be multithreaded
       - Multiple independent sequences of computational steps
	 - E.g., an online game: different threads for game AI, GUI
           events, network handling
       - Multi-core CPUs can execute multiple threads in parallel

** Thread Creation and Termination
   - Different OSes and different languages provide different APIs to
     manage threads
     - Thread creation
       - [[#Simpler2Threads][Following example]]: Java
       - cite:Hai19: Java and POSIX threads
     - Thread termination
       - API-specific functions to end/destroy threads
       - Implicit termination when “last” instruction ends
	 - E.g., [[#java-threads][in Java]] when methods ~main()~ (for
           main thread) or ~run()~ (for other threads) end (if
           at all)

** Thread Terminology
   :PROPERTIES:
   :CUSTOM_ID: thread-terminology
   :END:
   - [[#parallelism][Parallelism]] vs [[#concurrency][concurrency]]
   - [[#preemption][Thread preemption]]
   - [[#classification][I/O bound vs CPU bound threads]]

*** Parallelism
    :PROPERTIES:
    :CUSTOM_ID: parallelism
    :END:
    #+INDEX: Parallelism (Threads)
    - *Parallelism* = *simultaneous* execution
      - E.g., multi-core
      - Potential speedup for computations!
	- (Limited by
          [[beyond:https://en.wikipedia.org/wiki/Amdahl%27s_law][Amdahl’s law]])
    - Note
      - Processors contain more and more cores
      - Individual cores do not become much faster any longer
	- Recall CS part about [[basic:https://en.wikipedia.org/wiki/Moore%27s_law][Moore’s law]]
      - Consequence: Need parallel programming to take advantage of
	current hardware

*** Concurrency
    :PROPERTIES:
    :CUSTOM_ID: concurrency
    :END:
    #+INDEX: Concurrency (Threads)
    - Concurrency is *more general* term than parallelism
      - Concurrency includes
	#+ATTR_REVEAL: :frag (appear)
	- *Parallel* threads (on multiple CPU cores)
          {{{reveallicense("./figures/OS/3-threads-parallel.meta","10rh",nil,t)}}}
	  - (Executing different code in general)
	- *Interleaved* threads (taking turns on single CPU core)
	  - With gaps on single core!
            {{{reveallicense("./figures/OS/3-threads-interleaved.meta","10rh",nil,t)}}}
      #+ATTR_REVEAL: :frag appear
      - Challenges and solutions for concurrency apply to parallel
        and interleaved executions
	- Topics covered in upcoming presentations
          ([[file:Operating-Systems-MX.org][mutual exclusion (MX)]],
          [[file:Operating-Systems-MX-Java.org][MX in Java]],
          [[file:Operating-Systems-MX-Challenges.org][MX challenges]])

*** Thread Preemption
    :PROPERTIES:
    :CUSTOM_ID: preemption
    :END:
    #+INDEX: Preemption!Definition (Threads)
    - *Preemption* = temporary removal of thread from CPU by OS
      - Before thread is finished (with later continuation)
	- To allow others to continue after *scheduling* decision by OS
      - Typical technique in modern OSs
	- Run lots of threads for brief intervals per second; creates
          illusion of parallel executions, even on single-core CPU
    - Later slides: [[#cooperative-mt][Cooperative]] vs
      [[#preemptive-mt][preemptive]] multitasking
    - Upcoming presentation: [[file:Operating-Systems-Scheduling.org][Thread scheduling]]

*** Thread Classification
    :PROPERTIES:
    :CUSTOM_ID: classification
    :END:
    - *I/O bound*
      - Threads spending most time submitting and waiting for I/O
	requests
      - Run frequently by OS, but only for short periods of time
	- Until next I/O operation
	- E.g., [[#interleaved][virus scanner]], network server (e.g.,
          web, chat)
    - *CPU bound*
      - Threads spending most time executing code
      - Run for longer periods of time
	- Until preempted by scheduler
	- E.g., [[#interleaved][graph rendering]],
          [[basic:https://en.wikipedia.org/wiki/Compiler][compilation]]
          of source code,
          training for [[beyond:https://en.wikipedia.org/wiki/Deep_learning][deep learning]]

* Java Threads
** Threads in Java
   :PROPERTIES:
   :CUSTOM_ID: java-threads
   :reveal_extra_attr: data-audio-src="./audio/3-java-3-steps.ogg"
   :END:
   #+INDEX: Thread!Java (Threads)
   #+INDEX: Java threads (Threads)
   - Threads are created from *instances* of classes implementing the
     [[https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/lang/Runnable.html][Runnable]] interface
     1. Implement ~run()~ method
     2. Create new [[https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/lang/Thread.html][Thread]] instance from ~Runnable~ instance
     3. Invoke ~start()~ method on ~Thread~ instance
   #+ATTR_REVEAL: :frag appear
   - Alternatives (beyond the scope of this course)
     - Subclass of ~Thread~ (~Thread implements Runnable~)
       - If more than ~run()~ overwritten
     - [[beyond:https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/util/concurrent/Executor.html][java.util.concurrent.Executor]]
       - With ~Callable<V>~, ~Future<V>~ and service methods of ~Executor~
	 - [[#worker-thread-pool][Worker Thread Pool]]
   #+begin_notes
   Here you see one way for the creation of threads in Java with 3 basic steps.
   First, the code to be executed by a thread is specified by
   the method ~run()~ of the interface ~Runnable~, and a thread itself
   is represented as instance of a class that implements this interface.

   Note that programmers do not call ~run()~ directly: Instead, step (2)
   requires to create an instance of class ~Thread~, which is then
   started in step (3) with method ~start()~, which in turn eventually
   calls ~run()~.

   The next slide shows an example for these three steps.
   #+end_notes

** Java Thread Example
  :PROPERTIES:
  :CUSTOM_ID: Simpler2Threads
   :reveal_extra_attr: data-audio-src="./audio/3-Simpler2Threads.ogg"
  :END:
   #+INDEX: Thread!Java example (Threads)

   #+INCLUDE: "./java/Simpler2Threads.java" src java

   #+begin_notes
   Connecting Java threads to earlier topics, when you execute ~java~,
   the OS creates a process for the Java runtime.  Within that process,
   one thread will be created to execute the ~main()~ method.  In
   addition, the runtime creates an implementation-specific number of
   threads for Java management tasks.  Such threads are not important
   for our purposes.

   The program Simpler2Threads  does not only run code in the main
   thread, but it is multi-threaded, as ~start()~ is invoked on the
   new ~Thread~ instance ~childThread~ (and ~start()~ automatically
   calls ~run()~ to execute the thread’s code).

   Importantly, the method ~sleep()~ used here involves a
   [[file:Operating-Systems-Interrupts.org::#blocking][blocking]]
   system call to the OS, essentially stating “please take me away for
   the specified amount of milliseconds and give the CPU to someone else”.

   Which output do you expect at what points in time?
   #+end_notes

** JiTT Task: CPU Usage
   :PROPERTIES:
   :reveal_data_state: no-toc-progress
   :reveal_extra_attr: class="jitt"
   :END:
   This task is available for self-study in {{{jittquiz}}}.

   - Compile and run the
     [[https://gitlab.com/oer/OS/-/blob/master/java/Simpler2Threads.java][source code]]
     of [[#Simpler2Threads][Simpler2Threads]] and make sure that you
     can explain its output.  Maybe ask in {{{jittquiz}}}.
     - In general, if a program behaves unexpectedly, a debugger
       helps to understand what is happening when.  Your favorite IDE
       probably includes a debugger.  Also, several Java implementations
       come with a simple debugger called ~jdb~, e.g., the
       [[http://openjdk.java.net/tools/][OpenJDK tools]].
       (The notes on this slide contain
       sample commands for ~jdb~.)
#+BEGIN_NOTES
- jdb Simpler2Threads
- stop in Simpler2Threads.main
- run
- threads
- stop in MyThread.run
- step
- threads
#+END_NOTES

* Reasons for Threads
** Main Reasons
   :PROPERTIES:
   :CUSTOM_ID: thread-reasons
   :END:
   - *Resource utilization*
     - Keep most of the hardware resources busy most of the time, e.g.:
       - While one thread is *idle* (e.g., waiting for external events
         such as user interaction, disk, or network I/O), allow other
         threads to continue
	 - [[#interleaved][Next slide]]
       - Keep multiple CPU cores busy
	 - E.g., OS housekeeping such as zeroing of memory on second core
   - *Responsiveness*
     - Use separate threads to react quickly to external events
       - Think of game AI vs GUI
       - Other example on later slide: [[#webserver][Web server]]
   - More *modular design*

** Interleaved Execution Example
   :PROPERTIES:
   :CUSTOM_ID: interleaved
   :reveal_extra_attr: data-audio-src="./audio/f0206a.ogg"
   :END:
   #+INDEX: Interleaved execution (Threads)
   {{{reveallicense("./figures/OS/hail_f0206.svg.meta","50rh",nil,nil,t)}}}
#+BEGIN_NOTES
This figure illustrates the benefit of improved resource utilization
resulting from multithreading, which leads to higher overall throughput.
Consider two threads and their resource demands, each taking 1h to finish.
The first thread, shown on the left, is I/O bound, in this case a
virus scanner, which uses the CPU only for brief periods of time,
whereas it mostly waits for new data to arrive from disk.
In contrast, the other thread, shown to the right, is CPU bound,
performing complex graph rendering; it doesn’t need the disk at all.
Clearly, the sequential execution of both threads, which takes 2h,
is a waste of resources, namely a waste of CPU time.

In fact, an OS that is equipped with a scheduling mechanism might be
able to schedule the 2nd thread whenever the 1st one is idle waiting for
new data to arrive from disk.  In that case, both threads can be executed
in an interleaved fashion on a single CPU core, keeping the core busy
all the time.  In the example shown here, both threads now finish
after 1.5h.

Note that the total time of 1.5h is an arbitrary example,
without underlying calculation.  The point is that idle times of the
virus scanner can now be used for real work, which leads to a total
time of less than 2h.  Of course, both threads could also finish at
different points in time (but earlier than 2h).
#+END_NOTES

** Example: Web Server
  :PROPERTIES:
  :CUSTOM_ID: webserver
  :END:
   - Web server “talks” HTTP with browsers
   - Simplified
     - Lots of browsers ask per GET method for various web pages
     - Server responds with HTML files
   - How many threads on server side?

** Single- vs Multithreaded Web Server
  :PROPERTIES:
  :CUSTOM_ID: fig2.5
  :reveal_extra_attr: data-audio-src="./audio/f0205.ogg"
  :END:
   {{{reveallicense("./figures/OS/hail_f0205.pdf.meta","50rh")}}}
#+BEGIN_NOTES
This figure illustrates a thought experiment.

Suppose that you implemented a web server using a single thread.
When a browser connects, it typically asks for a sequence of resources
such as images, CSS, JavaScript, and HTML files.
These resources need to be retrieved from disk and transmitted over
the Internet, before an entire page can be rendered by the browser.
If the server processes this entire sequence before turning to the
next client, complex pages, network latency, and slow clients will
cause long delays for other clients.
Consequently, web servers are not built this way.

At another extreme, which is also not used in practice for reasons to
be discussed later, a multithreaded server could create a new thread
to handle each incoming request separately.
That way, requests can be processed in parallel or concurrently, which
improves responsiveness and resource usage.
In particular, the server would no longer be slowed down by slow clients.
#+END_NOTES


* Thread Switching
** Thread Switching for Multitasking
   :PROPERTIES:
   :CUSTOM_ID: thread-switching
   :reveal_extra_attr: data-audio-src="./audio/3-thread-switching.ogg"
   :END:
   #+INDEX: Thread!Switching (Threads)
   #+INDEX: Multitasking!Thread switching (Threads)
   #+INDEX: Context switch!Scheduling (Threads)
   - With multiple threads, OS decides which to execute when
     → Scheduling ([[file:Operating-Systems-Scheduling.org][later lecture]])
     - (Similar to machine scheduling for industrial production, which
       you may know from operations management)
     - [[file:Operating-Systems-Introduction.org::#multitasking][Recall multitasking]]
       - OS may use time-slicing to schedule threads for short
         intervals, illusion of parallelism on single CPU core
   - After that decision, a *context switch* takes place
     - (Recall
       [[file:Operating-Systems-Introduction.org::#user-space-kernel-space][introduction]]
       and
       [[file:Operating-Systems-Interrupts.org::#irq-handling][interrupts]];
       now, as *thread switch*)
     - Remove thread A from CPU
       - *Remember A's state* (in [[#tcb][TCB]]: instruction pointer,
	 register contents, stack, …)
     - *Dispatch* thread B to CPU
       - *Restore B's state*
   #+begin_notes
Recall how code is executed on the CPU (e.g., with Hack).  A special
register, the program counter, specifies what instruction to execute
next, and instructions may modify CPU registers.  You may think of one
assembly language program as being executed in one thread.

Recall that with
[[file:Operating-Systems-Introduction.org::#multitasking][multitasking]],
the OS manages multiple threads and schedules them for execution on
CPU cores, usually with time-slicing.

If the time slice for thread A ends, A is usually in the middle of
some computation.  The state of that computation is defined by the
current value of the program counter, by values stored in registers,
and other information.  To resume this computation later on, the OS
needs to save the state of thread A somewhere, before another thread B
can be executed.  Similarly, thread B may be in the middle of its own
computation, whose state was saved previously by the OS.

The switch from thread A via the OS to thread B with saving of A’s and
restoring of B’s state is called a /context switch/.  Subsequent
slides provide more details how such context switches happen.
   #+end_notes

*** Thread Switching with ~yield~
    :PROPERTIES:
    :CUSTOM_ID: yielding
    :END:
    #+INDEX: Thread!Yield (Threads)
    #+INDEX: Yielding (Threads)
    In the following
    - First, simplified setting of voluntary switch from thread A to
      thread B
      - Function ~switchFromTo()~ on [[#interleaved-instructions][next slide]]
	- For details, see Sec. 2.4 in cite:Hai19
      - Leaving the CPU voluntarily is called *yielding*; ~yield()~ may
	really be an OS system call
    - Afterwards, the real thing: *Preemption* by the OS

** Interleaved Instruction Sequence
   :PROPERTIES:
   :CUSTOM_ID: interleaved-instructions
   :END:

   {{{revealimg("./figures/OS/3-interleaved-executions.meta",t,"50rh")}}}

** Thread Control Blocks (TCBs)
   :PROPERTIES:
   :CUSTOM_ID: tcb
   :END:
   #+INDEX: Thread control block (TCB) (Threads)
   #+INDEX: Stack!TCB (Threads)
   - All threads share the same CPU registers
     - Obviously, register values need to be *saved* somewhere to avoid
       incorrect results when switching threads
     - Also, each thread has its own
       - *stack*; current position given by *stack pointer (SP)*
       - *instruction pointer (IP)* (program counter); where to execute next machine instruction
     - Besides: priority, scheduling information, blocking events (if any)
   - OS uses block of memory for housekeeping,
     called *thread control block* (TCB)
     - One for each thread
       - Storing register contents, stack pointer, instruction
         pointer, …
     - Arguments of ~switchFromTo()~ are really (pointers to) TCBs

** Cooperative Multitasking
   :PROPERTIES:
   :CUSTOM_ID: cooperative-mt
   :END:
   #+INDEX: Multitasking!Cooperative (Threads)
   - Approach based on ~switchFromTo()~ is *cooperative*
     - Thread A decides to yield CPU (voluntarily)
       - A hands over to B
   - Disadvantages
     - Inflexible: A and B are hard-coded
     - No parallelism, just interleaved execution
     - What if A contains a bug and enters an infinite loop?
   - Advantages
     - Programmed, so full control over when and where of switches
     - Programmed, so usable even in restricted environments/OSs
       without support for multitasking/preemption

** Preemptive Multitasking
   :PROPERTIES:
   :CUSTOM_ID: preemptive-mt
   :END:
   #+INDEX: Preemption!Multitasking (Threads)
   #+INDEX: Multitasking!Preemptive (Threads)
   #+INDEX: Thread!Preemption (Threads)
   - [[#preemption][Preemption]]: OS removes thread *forcefully* (but only
     temporarily) from CPU
     - Housekeeping on stacks to allow seamless continuation later on
       similar to cooperative approach
     - OS schedules different thread for execution afterwards
   - Additional mechanism: Timer interrupts
     - OS defines *time slice* (*quantum*), e.g., 30ms
       - Interrupt fires every 30ms
       - Interrupt handler invokes OS scheduler to determine next thread
	 - Details in [[file:Operating-Systems-Scheduling.org][upcoming presentation]]

** Multitasking Overhead
   :PROPERTIES:
   :CUSTOM_ID: multitasking-overhead
   :END:
   #+INDEX: Multitasking!Overhead (Threads)
   #+INDEX: Multitasking!Overhead (Threads)
   #+INDEX: Preemption!Overhead (Threads)
   #+INDEX: Overhead (Threads)
   #+INDEX: Caching!Cache pollution (Threads)
   - OS performs scheduling, which takes time
   - Thread switching creates *overhead*
     - Minor sources: Scheduling costs, saving and restoring state
     - Major sources: Recall [[file:Operating-Systems-Interrupts.org::#real-cpus][cache pollution]]
       - After a context switch, the CPU’s cache quite likely misses
         necessary data
	 - Necessary data needs to be fetched from RAM
       - Accessing data in RAM takes hundreds of clock cycles
	 - See [[https://stackoverflow.com/questions/4087280/approximate-cost-to-access-various-caches-and-main-memory][estimates on Stack Overflow]]


* Server Models
** Server Models with Blocking I/O: Single-Threaded
   :PROPERTIES:
   :CUSTOM_ID: single-threaded-server
   :reveal_extra_attr: data-audio-src="./audio/3-server-single-thread.ogg"
   :END:
   - *Single thread* (left-hand side of [[#fig2.5][Figure 2.5]]; thought experiment,
     not for use)
     {{{reveallicense("./figures/OS/hail_f0205.pdf.meta","20rh")}}}
   - *Sequential* processing of client requests
     - Long idle time during I/O
       @@html:<br>@@ → Not suitable in practice
   #+begin_notes
Recall Figure 2.5, where you saw that using a single thread to serve
lots of clients may lead to long delays.

Think of the thread as a single employee in a self-service restaurant,
who always processes each client entirely (e.g., order, cooking, payment)
before turning to the next client.
   #+end_notes

** Server Models with Blocking I/O: Multi-Threaded
   :PROPERTIES:
   :CUSTOM_ID: multi-threaded-server
   :reveal_extra_attr: data-audio-src="./audio/3-server-thread-per-connection.ogg"
   :END:
   - One *thread* (or process) *per client connection*
   - *Parallel* processing of client connections
     - No idle time for I/O (switch to different client)
     - Limited scalability (thousands or millions of clients?)
       - Creation of threads causes overhead
	 - Each thread allocates resources
       - OS needs to identify “correct” thread for incoming data
   - [[#worker-thread-pool][Worker Thread Pool]] as compromise
   #+begin_notes
In a conceptually simple server model, a new thread is created to
serve each incoming connection.  Thus, different clients can be served
in parallel, keeping all CPU cores busy (if enough connections are
active).  Also, slow clients do not slow down other clients, and the
OS can take a thread waiting for I/O aside and allow another thread to
execute instead.  Think of a hypothetical self-service restaurant,
where each customer is served by their own employee.

However, each thread needs some resources (e.g., RAM) and needs to be
managed by the OS.  Also, when data arrives in an ongoing connection,
the OS first needs to identify the correct thread to handle incoming
data before then performing a context switch to the correct thread.
These types of overhead limit the scalability of this model, which is
why the compromise of a Worker Thread Pool exists, to be discussed later.
   #+end_notes

** Server Models with Non-Blocking I/O
   :PROPERTIES:
   :CUSTOM_ID: non-blocking-server
   :reveal_extra_attr: data-audio-src="./audio/3-server-non-blocking.ogg"
   :END:
   - Single thread
     - *Event* loop, event-driven programming
     - E.g., web servers such as [[beyond:https://www.lighttpd.net/][lighttpd]], [[beyond:https://www.nginx.com/][nginx]]
   - Finite automaton to keep track of state per client
     - State of automaton records state of interaction
     - Complex code
       - See Google’s experience mentioned in cite:B17
   - Avoids overhead of context switches
     - Scalable (may be combined with [[#worker-thread-pool][Worker Thread Pool]])
   #+begin_notes
Another option to implement servers lies in the use of non-blocking
I/O operations, which is used by web servers such as lighttpd or
nginx.  Here, a single thread serves lots of incoming requests in an
interleaved fashion.

Processing of a request starts as in any other model.  When I/O is
necessary, e.g., to fetch HTML code from disk before network transfer,
the thread executes a non-blocking I/O operation.  Thus, the OS does
/not/ take the thread aside (as it would for blocking I/O operations)
but immediately returns an incomplete result.  (Think of you as thread
in a self-service restaurant.  You place your order and receive some
receipt or order number instead of your meal.)

Hence, the thread sees that it should do something else for some time.
So, the thread remembers the state of the current interaction,
typically in some finite automaton, and continues to work on another
request.  (You might take the receipt in the self-service restaurant
and continue your work on a JiTT task while your meal is prepared.
In addition, employees taking orders can again be perceived as
threads with non-blocking calls into the kitchen; while meals are
prepared, employees turn to other customers.)

This model avoids the overhead of context switches as a single thread
can continue with 100% CPU usage.  (You do not just sit there, wasting
your time, but you order, work on JiTT, eat, etc.)

Also, we can create one thread per CPU core for parallel processing
with up to 100% CPU utilization across all cores.

However, server code is complex and error prone.  The research paper
cited here includes experiences at Google, stating that blocking “code
is a lot simpler, hence easier to write, tune, and debug.”
   #+end_notes

** Worker Thread Pool
   :PROPERTIES:
   :CUSTOM_ID: worker-thread-pool
   :reveal_extra_attr: data-audio-src="./audio/3-worker-thread-pool.ogg"
   :END:
   #+INDEX: Worker thread pool (Threads)
   - Upon program start: Create *set* of *worker* threads
   - Client requests received by *dispatcher* thread
     - Requests recorded in to-do data structure
   - Idle worker threads process requests
   - Note
     - *Re-use* of worker threads
     - *Limited* resource usage
     - How to tune for load?
       - Dispatcher may be bottleneck
       - If more client requests than worker threads, then potentially
         long delays
   #+begin_notes
With worker thread pools, a fixed number of threads, namely one
dispatcher and several workers, are created ahead of time.  The
dispatcher just records incoming requests in a to-do data structure.

If a worker is currently idle (has nothing to do), it checks whether
the to-do list contains requests.  If so, the worker picks one,
removes it from the list, and processes it.

Think of a self-service restaurant with a single employee as dispatcher
and multiple cooks as workers.

With this model, the overhead compared to the thread-per-connection is
limited as (a) the number of threads is fixed and (b) workers do not
need to be created and destroyed dynamically.  Also note that
dispatcher and workers can be assigned to different CPU cores for
parallel work.  It may happen, though, that dispatcher or workers are
overloaded by the number of incoming requests, which leads to
potentially long delays (again, think of self-service restaurants).
   #+end_notes

* Conclusions
** Summary
   - Threads represent individual instruction execution sequences
   - Multithreading improves
     - Resource utilization
     - Responsiveness
     - Modular design in presence of concurrency
   - Preemptive multithreading with housekeeping by OS
     - Thread switching with overhead
   - Design choices: I/O blocking or not, servers with multiple
     threads or not

#+INCLUDE: "backmatter.org"
