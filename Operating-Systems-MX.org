# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config-course.org"
#+MACRO: jittquiz [[https://sso.uni-muenster.de/LearnWeb/learnweb2/course/view.php?id=60751#section-9][Learnweb]]

#+TITLE: OS05: Mutual Exclusion
#+SUBTITLE: Based on Chapter 4 of cite:Hai19
#+KEYWORDS: operating system, mutual exclusion, race condition, critical section, mutex, lock, semaphore
#+DESCRIPTION: First of three presentations on mutual exclusion in the context of operating systems.  Introduces mechanisms for mutual exclusion of critical sections in concurrent computations

* Introduction

# This is the sixth presentation for this course.
#+CALL: generate-plan(number=6)

** Today’s Core Questions
   - What can go wrong with concurrent computations?
     - What is a race condition?
   - How to avoid subtle programming bugs related to timing issues?
     - What mechanisms does the OS provide to help?

** Learning Objectives
   - Recognize and explain race conditions
     - (Playing [[basic:https://deadlockempire.github.io/][Deadlock Empire]] helps)
   - Explain notions of critical section and mutual exclusion
   - Use mutexes and semaphores (and monitors after upcoming lectures)
     to enforce mutual exclusion

** Retrieval Practice
*** Informatik 1
    You have [[file:Operating-Systems-Threads.org::#cs101][seen this advice before]].
    It is repeated here for emphasis:

    #+ATTR_REVEAL: :frag appear
    - What are *interfaces* and *classes* in Java, what is
      [[https://docs.oracle.com/javase/tutorial/java/javaOO/thiskey.html][this]]?
    - If you are not certain, consult a textbook; these self-check
      questions and preceding tutorials may help:
      - https://docs.oracle.com/javase/tutorial/java/concepts/QandE/questions.html
      - https://docs.oracle.com/javase/tutorial/java/IandI/QandE/interfaces-questions.html

*** Recall: Datenmanagement
    :PROPERTIES:
    :CUSTOM_ID: transaction
    :END:
    - Give examples for *dirty read* and *lost update* anomalies.
    - What is a database *transaction*?
    - What does each of the four *ACID guarantees* mean?
    - Explain *serializability* as notion of consistency.
    The above is covered in
    [[beyond:https://chris-olbrich.gitlab.io/emacs-reveal-transaction-processing/transactionprocessing.html][this introduction to transaction processing]].

** Table of Contents
   :PROPERTIES:
   :UNNUMBERED: notoc
   :HTML_HEADLINE_CLASS: no-toc-progress
   :END:
#+REVEAL_TOC: headlines 1

* Race Conditions
** Central Challenge: Races
   :PROPERTIES:
   :reveal_data_state: no-toc-progress
   :END:
   {{{reveallicense("./figures/OS/race-crash-cc-by-2.0.meta")}}}

** Races (1/2)
   :PROPERTIES:
   :CUSTOM_ID: race-condition
   :END:
   #+INDEX: Race condition (MX I)
   - *Race (condition)*: a technical term
     - Multiple *activities* access *shared resources*
       - At least one writes, in [[file:Operating-Systems-Threads.org::#concurrency][parallel or concurrently]]
     - Overall outcome depends on *subtle timing* differences
       - “Nondeterminism” (recall [[file:Operating-Systems-Interrupts.org::#dijkstra][Dijkstra on interrupts]])
       - Missing synchronization
       - *Bug*!
   - Previous picture
     - *Cars* are activities
     - *Street segments* represent shared resources
     - Timing determines whether a *crash* occurs or not
     - Crash = misjudgment = missing synchronization

** Races (2/2)
   - DBMS
     - *SQL commands* are activities
     - *Database objects* are shared resources
     - *Update anomalies* indicate missing synchronization
       - Serializability requires synchronization and avoids anomalies
         - E.g., [[#transaction][ACID transactions]] via locking
   - OS
     - *Threads* are activities
     - *Variables, memory, files* are shared resources
     - Missing synchronization is a *bug*, leading to anomalies just as
       in database systems

** JiTT Tasks
   :PROPERTIES:
   :reveal_data_state: no-toc-progress
   :reveal_extra_attr: class="jitt"
   :END:

*** JiTT Assignment: The Deadlock Empire, Part 1
    :PROPERTIES:
    :CUSTOM_ID: deadlock-empire
    :reveal_data_state: no-toc-progress
    :reveal_extra_attr: class="jitt"
    :END:
    #+INDEX: Deadlock empire (MX I)
   - Play “Tutorial 1,” “Tutorial 2,” and the three games for
     “Unsynchronized Code” at [[basic:https://deadlockempire.github.io/]]
     - The games make use of C#
       - (Which you do not need to know; the games include lots of
	 explanations, also mouse-over helps)
   - General idea
     - The game is about *mutual exclusion* and *critical sections*,
       to be discussed next
       - At any point in time just *one* thread is allowed to execute
	 under mutual exclusion inside a critical section
       - If you manage to lead two threads into a critical section
	 simultaneously (or, in some levels, to execute ~Assert(false)~),
         you demonstrate a [[#race-condition][race condition]]
     - You *win* a game if you demonstrate a race condition
   #+ATTR_REVEAL: :frag appear
   - Really, start playing *now*! (Nothing to submit here)

*** Transfer of Deadlock Empire
    :PROPERTIES:
    :CUSTOM_ID: sell-race
    :reveal_data_state: no-toc-progress
    :reveal_extra_attr: class="jitt"
    :END:

    Consider the following piece of Java code (from Sec. 4.2 of cite:Hai19)
    to sell tickets as long as seats are available.  (That code is
    embedded in
    [[basic:https://gitlab.com/oer/OS/blob/master/java/TheaterEx.java][this sample program]],
    which you can execute to see races yourself.)

#+BEGIN_SRC java
if (seatsRemaining > 0) {
   dispenseTicket();
   seatsRemaining = seatsRemaining - 1;
} else displaySorrySoldOut();
#+END_SRC

    Inspired by the Deadlock Empire, find and explain a race condition
    involving the counter ~seatsRemaining~, which leads to single
    tickets being sold several times (to be revisited in exercises).

** Non-Atomic Executions
   :PROPERTIES:
   :CUSTOM_ID: atomicity
   :reveal_extra_attr: data-audio-src="./audio/5-atomicity.ogg"
   :END:
   #+INDEX: Atomicity!Atomic execution (MX I)
   - Races generally result from non-atomic executions
     #+ATTR_REVEAL: :frag (appear)
     - Even “single” instructions such as ~i += 1~ are *not atomic*
       - Execution via *sequences of machine instructions*
	 - Load variable's value from RAM
	 - Perform add in ALU
	 - Write result to RAM
     - A context switch may happen after any of these machine instructions,
       i.e., “in the middle” of a high-level instruction
       - Intermediate results accessible elsewhere
         - *No isolation* in the sense of [[#transaction][ACID transactions]]: races, dirty reads, lost updates
       - Demo: Play a game as instructed previously
#+BEGIN_NOTES
This slide highlights that even simple statements of high-level
programming languages are not executed atomically, which may be the source
of race conditions.

Note that the word “atomic” is used in its literal sense here.
So, an execution is *not* atomic if is really consists of multiple steps.

Be careful not to confuse this with the notion of atomicity of ACID
transactions.  In the ACID context, atomicity means that transactions
appear to be either executed entirely or not at all;
whether they consist of multiple steps or not is not an issue.
#+END_NOTES


* Critical Sections and Mutual Exclusion
** Goal and Solutions (1/3)
   :PROPERTIES:
   :CUSTOM_ID: mx-goal
   :END:
   #+INDEX: Mutual exclusion!Goal (MX I)
   #+INDEX: Critical section!Definition (MX I)
   - Goal
     - Concurrent executions that access *shared resources* should be
       *isolated* from one another
       - Cf. *I* in [[#transaction][ACID transactions]]
   #+ATTR_REVEAL: :frag appear
   - Conceptual solution
     - Declare *critical sections* (CSs)
       - CS = Block of code with potential for
         [[#race-condition][race conditions]] on shared resources
	 - Cf. transaction as sequence of operations on shared data
     - Enforce *mutual exclusion* (MX) on CSs
       - At most one thread inside CS at any point in time
         - This avoids race conditions
	 - Cf. serializability for transactions

** MX with CSs: Ticket example
   :PROPERTIES:
   :CUSTOM_ID: dispense-mx
   :reveal_extra_attr: data-audio-src="./audio/5-mx.ogg"
   :END:
   {{{reveallicense("./figures/OS/5-dispense-mx.meta","50rh",nil,nil,t)}}}
#+BEGIN_NOTES
0. The animation on this slide illustrates the effect of mutual
   exclusion on interleaved executions to avoid races based on a
   previously shown code fragment to sell tickets for seats.
   If you did not think about the JiTT assignment for that code fragment
   yet, please do so now and come back afterwards.  Actually, this
   animation may also help you solving the JiTT assignment.

   Consider two threads that simultaneously try to obtain seats, and
   suppose that the code fragment is executed as critical section
   under mutual exclusion.  How MX can actually be enforced via
   locking, semaphores, or monitors is the topic of later slides.

1. T1 enters the CS first.  Suppose its time slice runs out after the
   check that seats are still remaining.  Now the OS dispatches T2,
   which wants to execute the same CS.  However, as T1 is currently
   executing inside that CS and as MX is enforced for that CS, T2 is
   blocked by the OS.  Thus, the OS schedules another thread for
   execution, say T1.

2. Consequently, T1 can finish the CS without intermediate modifications
   by any other thread.

3. Afterwards, T2 can enter the CS, check whether seats are still
   available etc.  In essence, MX enforces the serial execution of
   CSs.
   On the one hand, serial executions are good as they avoids races;
   on the other, they inhibit parallelism, which is generally bad for
   performance.
#+END_NOTES

** Goal and Solutions (2/3)
   :PROPERTIES:
   :CUSTOM_ID: mx-goal-solution
   :END:
   #+INDEX: Mutual exclusion!Solutions (MX I)
   - New goal
     - Implementations/mechanisms for MX on CS
   - Solutions, to be discussed in detail
     - *Locks*, also called *mutexes*
       - Cf. 2PL for database [[#transaction][transactions]]
       - Acquire lock/mutex at start of CS, release it at end
         - Choices: Busy waiting (spinning) or blocking when lock/mutex not
           free?
     - *Semaphores*
       - Abstract datatype, generalization of locks, blocking, signaling
     - *Monitors*
       - Abstract datatype, think of Java class, methods as CS with MX
       - Keyword ~synchronized~ turns Java method into CS with MX!

** Challenges
   :PROPERTIES:
   :CUSTOM_ID: mx-challenges
   :END:
   #+INDEX: Mutual exclusion!Challenges (MX I)
   #+INDEX: Starvation (MX I)
   #+INDEX: Deadlock (MX I)
   - Above solutions restrict entry to CS
     - Thus, they restrict access to the resource CPU
   - Major synchronization challenges arise
     - *Starvation* (related to (un-) fairness)
       - Thread never enters CS
         - (More generally: never receives some resource, e.g.,
           [[file:Operating-Systems-Scheduling.org::#priority-starvation][CPU under scheduling]])
     - *Deadlock* (discussed [[file:Operating-Systems-MX-Challenges.org][in later presentation]])
       - Set of threads is stuck
       - Circular wait for additional
         locks/semaphores/resources/messages
     - In addition, programming errors, e.g., races involving
       [[#sell-race][~seatsRemaining~]]
       - Difficult to locate, time-dependent
       - Difficult to reproduce, “non-determinism”

** Goal and Solutions (3/3)
   :PROPERTIES:
   :CUSTOM_ID: mx-definition
   :END:
   #+INDEX: Mutual exclusion!Definition (MX I)
   - Recall above loose definition
     - MX = At most one thread inside CS at any point in time
       - This avoids race conditions
   - Stricter definitions also address deadlocks, starvation, failures
     - Our *definition*: Solution *ensures MX* if
       - At most one thread inside CS at any point in time
       - Deadlocks are ruled out
     - (Not our focus: Starvation does not occur)
       - (E.g., requests granted under fairness guarantees such as
         first-come first-serve or with “luck” based on randomness)
     - (cite:Lam86 provides detailed discussion, also addressing
       failures)

* Locking
  :PROPERTIES:
  :CUSTOM_ID: locking
  :END:

** Mutexes
   :PROPERTIES:
   :CUSTOM_ID: drawing-mutex
   :END:
   #+INDEX: Mutex!Drawing (MX I)
   {{{revealimg("./figures/external-non-free/jvns.ca/mutexes.meta",t,"60rh")}}}

** Locks and Mutexes
   :PROPERTIES:
   :CUSTOM_ID: locks-mutexes
   :reveal_extra_attr: data-audio-src="./audio/5-locks.ogg"
   :END:
   #+INDEX: Mutex!Definition (MX I)
   #+INDEX: Locking (MX I)
   - Lock = mutex = object with methods
     - ~lock()~ or ~acquire()~: Lock/acquire/take the object
       - A lock can only be ~lock()~'ed by one thread at a time
       - Further threads trying to ~lock()~ need to wait for ~unlock()~
     - ~unlock()~ or ~release()~: Unlock/release the object
       - Can be ~lock()~'ed again afterwards
     - E.g., interface [[beyond:https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/util/concurrent/locks/Lock.html][java.util.concurrent.locks.Lock]] in Java.

   {{{reveallicense("./figures/OS/hail_f0404.pdf.meta","20rh")}}}
#+BEGIN_NOTES
First of all, note that the terms “lock” and “mutex” are synonyms.

Locks (and mutexes) are special-purpose objects, which essentially
have two states, namely Unlocked and Locked, which you can see in the
figure here, along with possible state transitions when the lock’s
methods lock() and unlock() are invoked.  These methods are explained
in the bullet points.

When mutual exclusion (MX) is necessary to prevent races for a
critical section (CS), a lock object shared by the racing threads can
be used, for example seatlock on the next slide.  To enforce MX,
method lock() needs to be invoked on the lock object at the beginning
of the CS, and unlock() at the end.

When the first thread executes lock(), the locks’s state changes from
Unlocked to Locked.  If other threads try to execute lock() in state
Locked, these threads get blocked until the first thread executes
unlock(), which changes the lock’s state to Unlocked and which allows
the blocked threads to continue their locking attempts; of course,
only one of them will be successful.

The question whether the lock really changes its state from Locked to
Unlocked upon unlock() or whether it is immediately reassigned to one of
the blocked threads (e.g., in FIFO order) is a design decision, which
will be revisited later in the context of the so-called convoy problem.
#+END_NOTES

** Use of Locks/Mutexes
   :PROPERTIES:
   :CUSTOM_ID: lock-usage
   :END:
  - Programming discipline *required* to prevent races
    - Create one (shared) lock for each shared data structure
    - Take lock before operating on shared data structure
    - Release lock afterwards
  - (cite:Hai19 has sample code following POSIX standard)
  - Ticket example modified (leading to [[#dispense-mx][MX behavior]]):

#+BEGIN_SRC java
seatlock.lock();
if (seatsRemaining > 0) {
   dispenseTicket();
   seatsRemaining = seatsRemaining - 1;
} else displaySorrySoldOut();
seatlock.unlock();
#+END_SRC

** Quiz on MX Vocabulary
#+REVEAL_HTML: <script data-quiz="quizMX" src="./quizzes/mx.js"></script>

* Semaphores
** Semaphore Origin
   :PROPERTIES:
   :CUSTOM_ID: semaphore-origin
   :END:
   #+INDEX: Semaphore!Definition (MX I)
   #+INDEX: Atomicity!Semaphore operation (MX I)
   - Proposed by [[https://en.wikipedia.org/wiki/Edsger_W._Dijkstra][Dijkstra]], 1965
     - Based on *waiting* (sleeping) for *signals* (wake-up calls)
       - Thread waiting for signal is *blocked*
   #+ATTR_REVEAL: :frag appear
   - *Abstract data type*
     #+ATTR_REVEAL: :frag (appear)
     - Non-negative integer
       - Number of available resources; 1 for MX on CPU
     - Queue for blocked threads
     - *Atomic* operations
       - Initialize integer
       - ~acquire~ (wait, sleep, down, P [passeren, proberen]): entry into CS
	 - Decrement integer by 1
	 - As integer must be non-negative, *block* when 0
       - ~release~ (signal, wakeup, up, V [vrijgeven, verlaten]): exit
         from CS
	 - Increment integer by 1 (value may grow without bound)
	 - Potentially *unblock* blocked thread

** Use of Semaphores for MX
   - Programming discipline *required* [[#lock-usage][similarly to locks]]
     - Create semaphore for shared data structure
     - ~acquire()~ before CS, ~release()~ after
   - Ticket example modified with ~seatSem~ initialized to ~1~ (leading to [[#dispense-mx][MX behavior]]):
     - (The semaphore initialized to 1 behaves exactly like a lock here)
#+BEGIN_SRC java
seatSem.acquire();
if (seatsRemaining > 0) {
   dispenseTicket();
   seatsRemaining = seatsRemaining - 1;
} else displaySorrySoldOut();
seatSem.release();
#+END_SRC

*** Semaphores for Capacity Control
    - Semaphores initialized to other values than 1 are typically used
      to model *resource capacities*
      #+ATTR_REVEAL: :frag (appear)
      - Example from [[https://stackoverflow.com/a/40473][stack overflow: bouncer in nightclub]]
        - Nightclub has limited capacity, i.e., number of people allowed
          in club at any moment: /n/
          - Bouncer (semaphore initialized to /n/) makes sure that no more than /n/ people can be inside
          - If club is full, a queue collects waiting people
        - Guests (threads) call ~acquire()~ on bouncer/semaphore to enter
        - Guests (threads) call ~release()~ on bouncer/semaphore to leave
      - [[#semaphore-example][Example later on]]: ~SemaphoreBoundedBuffer~
      - (Semaphores are [[#semaphore-origin][defined above]]; as
        approximation, think of a semaphore initialized to /n/ as a
        set of /n/ mutexes/locks; each one protecting a different
        resource or part thereof)

** JiTT Tasks
   :PROPERTIES:
   :reveal_data_state: no-toc-progress
   :reveal_extra_attr: class="jitt"
   :END:

*** The Deadlock Empire -- Remarks
    :PROPERTIES:
    :reveal_data_state: no-toc-progress
    :reveal_extra_attr: class="jitt"
    :END:
    - The games at https://deadlockempire.github.io/ make use of C#
    - What is called “monitor” in C# is *not* a
      [[file:Operating-Systems-MX-Java.org::#java-monitors][Hoare style monitor]]
      to be discussed in the upcoming presentation
      - In C#, a “monitor” needs to be (un)locked explicitly
      - Whereas Hoare style monitors are locked implicitly and
        automatically
    - Class context
      - Hoare style [[file:Operating-Systems-MX-Java.org::#java-monitors][monitors in Java]]
      - Producer/consumer scenarios mentioned in games
	- [[#producer-consumer][Classical synchronization problems (this presentation)]]
	- Revisited as
          [[file:Operating-Systems-MX-Java.org::#java-bounded-buffer][BoundedBuffer in Java]]

*** The Deadlock Empire, Part 2
    :PROPERTIES:
    :reveal_data_state: no-toc-progress
    :reveal_extra_attr: class="jitt"
    :END:
    Play the following games at https://deadlockempire.github.io/
    - “Locks” and “Semaphores”
      - Incorrect use of both, sometimes leading to deadlocks.
	For locks, ~Enter()~ and ~Exit()~ represent ~lock()~ and ~unlock()~
    - “Condition Variables”
      - Here, ~if (queue.Count == 0)~ is meant to avoid removal
	attempts from empty queues. However, ~PulseAll()~ wakes up all
	waiting (blocked) threads (similarly to ~notifyAll()~ for Java
	later on)


* Implementation Aspects
** Atomic Instructions
   :PROPERTIES:
   :CUSTOM_ID: atomic-instruction
   :END:
   #+INDEX: Atomicity!Atomic machine instruction (MX I)
   - Typical building block for MX: [[#atomicity][Atomic]] machine instruction
     - Several memory accesses with guarantee of isolation/no interference
   - E.g., ~exchange~, which exchanges contents of register and
     memory location

** Mutex with Simplistic Spinlock Implementation
   :PROPERTIES:
   :CUSTOM_ID: spinlock-implementation
   :reveal_extra_attr: data-audio-src="./audio/5-spinlock-implementation.ogg"
   :END:
   #+INDEX: Spinlock!Implementation (MX I)
   #+INDEX: Mutex!Implementation with spinlock (MX I)
   #+INDEX: Lock!Implementation with spinlock (MX I)
   #+INDEX: Busy waiting (MX I)
   #+INDEX: Atomicity!Spinlock with atomic instruction (MX I)
   - Single memory location called ~mutex~
     - Value ~1~: unlocked
     - Value ~0~: locked
   - Operations
     - ~unlock()~: Store ~1~ into ~mutex~
     - ~lock()~: Atomically check for ~1~ and store ~0~ as follows
       (cite:Hai19):

#+BEGIN_SRC
to lock mutex:
  let temp = 0
  repeat
    atomically exchange temp and mutex
  until temp = 1
#+END_SRC

#+begin_notes
Consider a simplistic mutex implementation, where the mutex itself is
represented by a single memory location, called ~mutex~, which can
have the values 1 for unlocked and 0 for locked.  The operation
~lock()~ uses an [[#atomic-instruction][atomic machine instruction]]
to exchange the contents of memory location ~mutex~ with another value
(here ~temp~, which could be a CPU register).

Please convince yourself that out of multiple threads invoking
~lock()~ concurrently on the unlocked mutex, only the first one will
pass the ~repeat~ loop, while subsequent ones are stuck (or “spin”) in
that loop, until the first thread performs an unlock operation by
writing 1 into memory location ~mutex~.

This type of lock, where threads waiting for the release of a lock
spin in a loop, is called *spinlock* (to be [[#spinlocks][revisited shortly]]),
and it differs from locks where waiting threads are blocked by the OS,
which we assumed on earlier slides.  Recall that actively waiting in a
loop is also called *busy waiting* as we have seen in the context of
[[file:Operating-Systems-Interrupts.org::#polling-pro-con][I/O polling]].

Spinlocks are mainly used for low-level programming where the duration
of lock periods can be guaranteed to be short.
#+end_notes

#+REVEAL: split

See cite:Hai19 for a cache-conscious spinlock variant
(beyond scope of class).

** On Spinlocks
    :PROPERTIES:
    :CUSTOM_ID: spinlocks
    :END:
    #+INDEX: Spinlock!Discussion (MX I)
    #+INDEX: Busy waiting (MX I)
    - *Spinlock*: Thread spins *actively* in loop on CPU while waiting for
      lock to be released
      - *Busy waiting*
      - Avoids overhead of scheduling and context switch coming with
        blocking locks
    - Note: Spinning thread keeps CPU core busy
      - No blocking by OS
      - *Waste* of CPU resources unless lock periods are short

** Mutex with Queue
    :PROPERTIES:
    :CUSTOM_ID: mutex-queue
    :END:
    #+INDEX: Mutex!Implementation with queue (MX I)
    #+INDEX: Lock!Implementation with queue (MX I)
    - Mutex as OS mechanism
      - When ~lock()~ fails on a locked mutex, OS *blocks* thread
      - Blocked threads are collected in queue
      - *No* busy waiting
	- Thus, CPU time not wasted for long waiting periods
	- However, scheduling with its own *overhead* required
	  - Wasteful, if waiting periods are short
    - Different variants of ~unlock()~
      1. Unblock thread in FIFO order from queue (if one exists)
         - And reassign mutex to that thread
      2. Make all threads runnable without reassigning mutex
         - Upcoming presentation: [[file:Operating-Systems-MX-Challenges.org::#convoy][convoy problem]]
           and its [[file:Operating-Systems-MX-Challenges.org::#convoy-solution][solution]]
    - Pseudocode in cite:Hai19

** Quiz on Locking
#+REVEAL_HTML: <script data-quiz="quizLocking" src="./quizzes/locking.js"></script>


* Outlook
** Producer/Consumer problems
   :PROPERTIES:
   :CUSTOM_ID: producer-consumer
   :END:
   #+INDEX: Producer/consumer problems (MX I)
   - Classical synchronization problems, revisited in
     [[file:Operating-Systems-MX-Java.org::#java-bounded-buffer][next presentation]]
     - One or more *producers*
       - Generate data
	 - Records, messages, tasks
       - Place data into *buffer* (shared resource)
	 - Two buffer variants: unbounded or bounded
	 - Producer *blocks*, if bounded buffer is full
     - One or more *consumers*
       - Consume data
	 - Take data out of *buffer*
	 - Consumer *blocks*, if buffer is empty
   - Synchronization for *buffer manipulations* necessary

** Use of Semaphores to Track Resources
   :PROPERTIES:
   :CUSTOM_ID: semaphore-example
   :END:

#+INCLUDE: "./java/SemaphoreBoundedBuffer.java" src java

* Pointers beyond class topics
** GNU/Linux: Futex
   :PROPERTIES:
   :CUSTOM_ID: pi-futex
   :END:
   #+INDEX: Futex (MX I)
   - Fast user space mutex
     - No system call for single user (fastpath)
     - System calls for blocking/waiting (slowpath)
   - Meant as building block for libraries
   - Like semaphore: Integer with ~up()~ and ~down()~
     - Assembler code with atomic instructions for integer access
   - Documentation
     - ~man futex~
     - [[beyond:https://www.kernel.org/doc/Documentation/pi-futex.txt][PI-futex.txt]]
       - (Topic for upcoming presentation: PI stands for
         [[file:Operating-Systems-MX-Challenges.org::#priority-inheritance][priority inheritance]],
         a counter-measure against
         [[file:Operating-Systems-MX-Challenges.org::#priority-inversion][priority inversion]])

** Lock-free Data Structures
   :PROPERTIES:
   :CUSTOM_ID: lock-free
   :END:
   #+INDEX: Lock-free data structures (MX I)
   - Core idea: Handle critical sections without locks
   - Typically based on hardware support for atomicity guarantees
     - Atomic instructions as explained above
       - E.g., Bw-Tree, see cite:LLS13
     - Transactional memory, see cite:LK08
   - See Section 4.9 of cite:Hai19

** “Safer” Programming Languages
   - High-level programming languages may help with MX
   - See cite:JJK+21 for introduction to [[beyond:https://www.rust-lang.org/][Rust]]
     - Strong type system allows to detect common bugs at
       *compile* time
       - Thread safety (absence of race conditions) for shared data
         structures with compile-time checks
     - Ongoing research into safety proofs
   - (Besides, the OS [[beyond:https://www.redox-os.org/][Redox]] is implemented
     in Rust)

** Massively Parallel Programming
   - For massively parallel (big data) processing in clusters or cloud
     environments, specialized frameworks exist
     - Breaking down “large” tasks with
       [[basic:https://oer.gitlab.io/oer-courses/cacs/Distributed-Systems-Introduction.html#slide-partitioning][partitioning]]
       into smaller ones that are processed in parallel
       - Smaller tasks usually independent of each other (no race conditions)
       - (Built-in fault tolerance with
         [[basic:https://oer.gitlab.io/oer-courses/cacs/Distributed-Systems-Introduction.html#slide-replication][replication]])
     - E.g., [[beyond:https://en.wikipedia.org/wiki/Apache_Hadoop][Apache Hadoop (MapReduce)]],
       [[beyond:https://en.wikipedia.org/wiki/Apache_Spark][Apache Spark]],
       [[beyond:https://en.wikipedia.org/wiki/Apache_Flink][Apache Flink]]

* Conclusions
** Summary
   - Parallelism is a fact of life
     - Multi-core, multi-threaded programming
     - Race conditions arise
     - Synchronization is necessary
   - Mutual exclusion for critical section
     - Locking
     - Monitors
     - Semaphores

#+INCLUDE: "backmatter.org"
