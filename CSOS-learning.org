# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2018, 2021, 2022 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config-course.org"
#+LATEX_HEADER: \addbibresource{literature/education.bib}

# Create agenda slide with 1st-level headings.
#+OPTIONS: toc:1

# Do not show section numbers.
#+OPTIONS: num:nil

#+TITLE: Learning and Teaching

#+KEYWORDS: learning, teaching, active learning, deliberate practice
#+DESCRIPTION: Explanation of active learning and teaching with flipped classroom and JiTT

* Introduction
   #+ATTR_REVEAL: :frag (appear)
   1. Think of something you are really good at
      - Write it down (won’t be shared with anyone)
   2. Briefly describe how you got to be good at that thing
      - One or two words
   3. Submit how you got to be good at
      [[https://pingo.coactum.de/796643][Pingo]] (pingo.coactum.de → 796643)
      [[./img/qr-pingo-csos2022.png]]

   #+ATTR_HTML: :class slide-source
   (Source of activity: cite:SEI14)

* Learning
** Brain ≈ Muscle
   #+ATTR_REVEAL: :frag (appear)
   - Learning involves brain’s *long term memory*
     {{{reveallicense("./figures/3d-man/brain_teacher.meta")}}}
   - Long term memory needs *repeated* retrieval and practice
     - Spaced out *over time*
     - Effect: Changes in brain’s *proteins*
   - (Learning does *not* happen [solely] in lectures)

** Deliberate Practice
   Characteristics of *Deliberate Practice* to acquire expert skills
   (cite:Eri08, see also cite:EKT93,SEI14)

   1. Task with *well-defined goal*
   2. Individual *motivated* to improve
   3. *Feedback* on current performance
   4. Ample opportunities for *repetition* and *gradual refinements*

   #+ATTR_REVEAL: :frag (appear)
   [[color:gray][(Traditional lecturing is “teaching by telling”, does not share *any* characteristic of Deliberate Practice)]]
   #+begin_notes
   - First, go through enumeration
   - Then, refer back to practive vs talent
     - 10,000 hours to compete internationally in variety of domains
     - 10,000 h / 40h per week / 50 weeks = 5 years
   - Finally, “teaching by telling”
   #+end_notes

** Active Learning
   - *Active Learning* increases student performance in science,
     engineering, and mathematics (cite:FEM+14)
     - Active Learning is an umbrella term for diverse
       interventions
       - Group problem-solving
       - Worksheets or tutorials completed during class
       - Use of personal response systems with or without peer instruction
       - Studio or workshop course designs
     - Notice: Above interventions share at least 3 of the 4
       characteristics of Deliberate Practice
       - (Motivation may increase, but ultimately rests with *you*)

** Quotes from Experts
   - On cite:FEM+14
     - Carl Wieman, Nobel Prize in Physics 2001
       #+ATTR_REVEAL: :frag (appear)
       - “[[https://blogs.scientificamerican.com/budding-scientist/stop-lecturing-me-in-college-science/][A lecture is basically a talking textbook]]”
       - In cite:Wie14: “However, in undergraduate STEM education, we have the curious situation that, although more effective teaching methods have been overwhelmingly demonstrated, most STEM courses are still taught by lectures—the pedagogical equivalent of bloodletting.”
     #+ATTR_REVEAL: :frag appear
     - Eric Mazur, Harvard physicist
       - “[[https://www.sciencemag.org/news/2014/05/lectures-arent-just-boring-theyre-ineffective-too-study-finds][This is a really important article—the impression I get is that it’s almost unethical to be lecturing if you have this data]]”
   #+ATTR_REVEAL: :frag appear
   - cite:SR17: “Saying Goodbye to Lectures
     in Medical School—Paradigm Shift or Passing Fad?”
     - “60 slides in 45 minutes may seem like an efficient way to
       teach, but it is unlikely to be an effective way to learn”

* CSOS Approach
** Initial Problem and Improvement
   :PROPERTIES:
   :CUSTOM_ID: jitt-motivation
   :END:
   - 2016: Classroom response system revealed lack of student understanding
     - Yet, no in-class discussions, leaving me frustrated
       - Waste of our time
   - After introduction of JiTT: Situation improved

   {{{reveallicense("./figures/org/jitt/JiTT-Java-MX.meta","33rh",nil,none)}}}

** General Improvements
   :PROPERTIES:
   :CUSTOM_ID: jitt-improvements
   :END:
   {{{reveallicense("./figures/org/jitt/JiTT-Understanding.meta","60rh",nil,none)}}}

** CSOS Teaching History
   - In 2016, I taught CSOS in its entirety
     - With lots of in-class quizzes of questionable effect, as just
       explained
   - Subsequently, Prof. Dr. Vossen taught CS part, I OS
     - With slightly different flipped classroom approaches
   - Since 2021, I’m teaching CSOS again in its entirety
     - Again with different formats for CS and OS
       - Reuse of CS videos from 2020
       - HTML presentations such as this one for OS
       - With evaluation of both formats
     - With uniform use of JiTT quizzes in both parts, explained next

* Just-In-Time Teaching (JiTT)
** Overview
   - JiTT
     - Teaching and learning strategy based on web-based study
       assignments (self-learning) and active learner classroom
       - See [[https://en.wikipedia.org/wiki/Just_in_Time_Teaching][JiTT on Wikipedia]]
       - cite:MSN16 demonstrates improved learning for statistics courses
     - Instance of *active* learning, which leads to improved
       learning in general cite:FEM+14
     - Instance of flipped/inverted classrooms cite:LPT00,BV13
       - In-class and at-home events flipped
       - Individual computer-based instruction paired with in-class (group) activities
         - Individual learning shaped by individual background and preferences
         - Lectures to discuss questions and work on exercises

** Feedback Cycles with JiTT
   :PROPERTIES:
   :CUSTOM_ID: jitt-feedback-cycles
   :END:
   {{{reveallicense("./figures/teaching/JiTT-feedback-cycles.meta","60rh",nil)}}}

* Lessons Learned
** Sample Feedback
   - Misunderstandings
     - “JiTT destroys our freedom!”
     - “JiTT tasks are too difficult/open!”
   #+ATTR_REVEAL: :frag appear
   - Encouragement
     - “JiTT is/was a very good idea and was very helpful to understand
       the course’s content”
     - “The JiTT-Assignment in combination with the lecture helped to
       understand the topics a lot!”
     - “Please continue with this type of lecuture!”

** Benefits and Challenges
   #+ATTR_REVEAL: :frag (appear)
   - Benefits
     #+ATTR_REVEAL: :frag (appear)
     - Much more *fun* in meetings with prepared students
       - Sometimes unbelievably good solutions
     - JiTT tasks helped *tremendously* to identify misunderstandings
       and improve self-study material
       - From wording to new larger units
   - Challenges
     #+ATTR_REVEAL: :frag (appear)
     - Regarding students
       - Participation in class and in JiTT assignments
       - Workload expectations
     - Regarding myself
       - Increased awareness of hurdles for students

** On Last Year’s CSOS Evaluation
   - Only 23 students took part
     - Heterogeneous grade distribution
       - (See notes; from 4*1.0 to 1*4.0, neither 2.3 nor 5.0)
     - Sample quotes
       - Definitely my favourite Informatics module so far. I liked
         both parts, though I think the CS parts could use some
         reworking, either with HTML presentations aswell or with new videos.
       - Definitely one of the best lectures I visited.
       - I just don't see the point of the "non-lectures".
       - I dont liked JiTT and the HTML5 presentations.
     - Repeated issues
       - English with positive (practice) and negative (additional
         complexity) comments
       - Maybe too little material for CS, too much for OS
#+begin_notes
Grades: 1,1,1,1, 1.3,1.3, 1.7,1.7,1.7,1.7,1.7, 2,2,2,2,2, 2.7,2.7, 3,3, 3.3, 3.7, 4.0
#+end_notes

** Comments on Workload
   - CSOS is worth 9 credits
     - Almost a third of your weekly workload, maybe 12 hours per week
     - 12 hours = 8 * 90 minutes
       - 3 class sessions
       - Time for 5 sessions remaining
     - Suggestion: Reserve self-study time in your weekly schedules
       - Preparation of class topics
       - Exercise work

* Final Remarks
** On Expectations
   - cite:D+19 Actual learning vs feeling of learning
     #+ATTR_REVEAL: :frag appear
     - “[[https://www.eurekalert.org/pub_releases/2019-09/hu-lil090519.php][The effort involved in active learning can be misinterpreted as a sign of poor learning. On the other hand, a superstar lecturer can explain things in such a way as to make students feel like they are learning more than they actually are.]]”
     #+ATTR_REVEAL: :frag (appear)
     - [[https://clarissasorensenunruh.com/2019/09/13/statistics-in-education-its-harder-than-it-looks/][Questions regarding statistical rigor]]
     - One conclusion: Explain approach to students

** JiTT in CSOS 2022
   - Joint sessions shaped by you!
   - Default plan
     - Tuesday sessions for first two tasks of exercise sheets
     - Wednesday session for general Q&A on exercises
     - Thursdays for Q&A on sample solutions
   - Please use Learnweb for asynchronous discussions
     - MoodleOverflow worked well last year
   - In addition, anonymous pads for synchronous and asynchronous Q&A


#+INCLUDE: "backmatter.org"
