# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2017-2021 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config-course.org"
#+OPTIONS: toc:nil

#+LATEX_HEADER: \addbibresource{literature/education.bib}

#+TITLE: Just-in-Time Teaching
#+KEYWORDS: just-in-time teaching, JiTT, learning, self-study
#+DESCRIPTION: Description of Just-In-Time Teaching for Operating Systems

* Usage hints
#+REVEAL_HTML: <script data-quiz="quizUsageHints" src="./quizzes/usage-hints.js"></script>

* Motivation
** Initial Problem and Improvement
   :PROPERTIES:
   :CUSTOM_ID: jitt-motivation
   :reveal_extra_attr: data-audio-src="./audio/jitt.ogg"
   :END:
   - 2016: Classroom response system revealed lack of student understanding
     - Yet, no in-class discussions, leaving me frustrated
       - Waste of our time
   - After introduction of JiTT: Situation improved

   {{{reveallicense("./figures/org/jitt/JiTT-Java-MX.meta","33rh",nil,none)}}}
   #+begin_notes
I started with Just-in-Time Teaching (JiTT) in 2017, after I realized
in the previous year that my mode of lecturing was a waste of our time
(students’s time as well as mine).  In particular, a classroom
response system revealed that students did not understand what I
wanted them to understand.

Here, you see results concerning a question that I have been asking
since 2016.  You will learn about its topic, mutual exclusion (MX), in
this course, and I expect you to be able to explain that concept.
In 2016, with traditional lecturing, I succeeded for about a third of
students, while the majority was confused but unwilling to ask
questions in class.  This in itself is bad.

Even worse, let me point out that students were quite happy with the
course, as indicated by the average evaluation grade of 1.8 (on a
German scale from 1.0 as best grade to 5.0).  This outcome raises deep
questions about learning and teaching.

As shown in the figure, results improved considerably with JiTT.

Note that I did not just switch to JiTT.  I learnt a lot about
learning, for which you can find some pointers in
[[file:CSOS-learning.org][this presentation]]; important key words are
deliberate practice, active learning, and self-regulation.

In this regard, note that /actual/ learning involves effort, while
evaluation results might reflect a /feeling/ of learning.  This is
explored in some detail in cite:D+19, about which the lead author
Louis Deslauriers says this:
“[[https://www.eurekalert.org/pub_releases/2019-09/hu-lil090519.php][The
effort involved in active learning can be misinterpreted as a sign of
poor learning. On the other hand, a superstar lecturer can explain
things in such a way as to make students feel like they are learning
more than they actually are.]]”
   #+end_notes

** General Improvements
   :PROPERTIES:
   :CUSTOM_ID: jitt-improvements
   :reveal_extra_attr: data-audio-src="./audio/jitt-improvements.ogg"
   :END:
   {{{reveallicense("./figures/org/jitt/JiTT-Understanding.meta","60rh",nil,none)}}}
   #+begin_notes
Students reported with overwhelming majority that JiTT has positive
effects on their learning.  Some students are not happy, though.
If you belong to that group, please talk to me.
   #+end_notes

* Just-In-Time Teaching (JiTT)
** Overview
   - Teaching and learning strategy based on web-based study
     assignments (self-learning) and active learner classroom
     - See [[https://en.wikipedia.org/wiki/Just_in_Time_Teaching][JiTT on Wikipedia ]]
     - cite:MSN16 demonstrates improved learning for statistics courses
   - JiTT is an instance of *active* learning, which leads to improved
     learning in general cite:FEM+14

** Feedback Cycles with JiTT
   :PROPERTIES:
   :CUSTOM_ID: jitt-feedback-cycles
   :reveal_extra_attr: data-audio-src="./audio/jitt-feedback.ogg"
   :END:
   {{{reveallicense("./figures/teaching/JiTT-feedback-cycles.meta","55rh",nil)}}}
   #+begin_notes
The essence of my interpretation of JiTT is captured in this figure,
which features three feedback loops:
1. Online discussions, e.g., in the Learnweb forum as in every other
   course
2. My feedback to your answers to JiTT questions
3. Focused discussions, shaped by what I learnt in JiTT answers

In pre-corona times, not many students attended class meetings.
Evaluation results show two major reasons.  First, students reported
that their questions were answered ahead of time, leaving little value
to class meetings.  Indeed, some students even suggested that I would
need to decrease the quality of provided material if I wanted more
students to attend.  I won’t go there.  Second, some students
decided to learn independently of our class rhythm.

In view of those results, I suggest not to rely on synchronous class
discussions in 2020 too much.  Instead, I suggest that you formulate
your questions online when they come up.

On a personal note, I believe that synchronous meetings are most
effective for small groups.  As a class, we are no small group.
In contrast, your group work will certainly benefit from synchronous
meetings (with discussions of topics that are relevant to /you/,
supported by /your/ preferred tools at /your/ preferred points in
time).

I’m here to help.
   #+end_notes

** Benefits
   #+ATTR_REVEAL: :frag (appear)
   - *Feedback loop*: Your out-of-class preparations are visible to
     me, allow me to offer feedback
   - More *structure* for out-of-class learning
     - Content and questions, to be tackled at individual learning pace
   - More efficient interaction
     - Identification and correction of
       misconceptions/misunder-standings/incorrect prior beliefs
     - Valuable *shared* time is not used for one-way lecturing but for
       student-student and student-instructor interactions
       - Traditionally, you figure out what's complicated when you are
         on your own
       - Now, we discuss once you found out what's complicated

* JiTT Organization

** JiTT Assignments
   - Upcoming presentations will contain assignments to be submitted by
     you via {{{learnweb}}}
     - Exercises
       - Usual deadlines on Thursdays as in CS part
     - But also voluntary assignments with earlier deadlines
       - *Tuesdays at 8am*
       - *Thursdays at 10am*
       - Giving me some time to check solutions ahead of scheduled meetings
       - Voluntary assignments serve as formative
         assessment; points without impact on grading
       - Voluntary assignments include one task to solicit
         questions/comments
         - Next slide

** How to obtain Feedback?
   - Each JiTT assignment ends with this task:
     - {{{understandingquestion}}}

** Asking for Help
   :PROPERTIES:
   :CUSTOM_ID: help
   :END:
   - Some students struggle on their own for hours,
     getting frustrated
     #+ATTR_REVEAL: :frag appear
     - Asking on so-called social networks where I am not around
       #+ATTR_REVEAL: :frag appear
       - Why do they do that?  Seems inefficient to me.
     #+ATTR_REVEAL: :frag appear
     - Asking search engines
     - Consulting YouTube (sometimes with faulty explanations)
   #+ATTR_REVEAL: :frag appear
   - I suggest to ask (and answer) *earlier* and *elsewhere*
     - In {{{learnwebforum}}}
     #+ATTR_REVEAL: :frag appear
     - A collaboratively edited document, URL in Learnweb

** Sample Student Feedback
*** Negative Feedback and my Responses
    - “JiTT destroys our freedom!”
      #+ATTR_REVEAL: :frag appear
      - Two meetings per week are given, *define* rhythm
        - You may adopt that rhythm, benefit from my help
        - Or explore alone at your own pace
    #+ATTR_REVEAL: :frag appear
    - “JiTT tasks are too difficult/open!”
      #+ATTR_REVEAL: :frag appear
      - CSOS is worth 9CP, almost a third of term’s workload
      - I do not just want you to remember my steps
        - I hope to instruct for independent movement in unfamiliar terrain
        - With challenging (I hope) hurdles and individual feedback
          - Missteps are part of learning
          - I’m here to [[#help][help]]
          - If you ask early, you may receive help before
            deadlines are due

*** Positive Feedback
    - “JiTT is/was a very good idea and was very helpful to understand
      the course’s content”
    - “The JiTT-Assignment in combination with the lecture helped to
      understand the topics a lot!”
    - “Please continue with this type of lecture!”

#+INCLUDE: "backmatter.org"

# Local Variables:
# indent-tabs-mode: nil
# End:
