# Local IspellDict: en
#+STARTUP: showeverything
#+SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+INCLUDE: "config-course.org"
#+MACRO: jittquiz [[https://sso.uni-muenster.de/LearnWeb/learnweb2/course/view.php?id=60751#section-9][Learnweb]]

#+TITLE: OS04: Scheduling
#+SUBTITLE: Based on Chapter 3 of cite:Hai19
#+KEYWORDS: operating system, scheduling, thread state, thread scheduling, CPU scheduling, round robin, priority
#+DESCRIPTION: Introduction to CPU scheduling in operating systems

* Introduction

# This is the fifth presentation for this course.
#+CALL: generate-plan(number=5)

** Previously on OS …
   - What is [[file:Operating-Systems-Introduction.org::#multitasking][multitasking]]?
   - What are [[file:Operating-Systems-Interrupts.org::#blocking][blocking]] system calls?
   - What [[file:Operating-Systems-Threads.org::#classification][types of threads]] exist?
   - What is [[file:Operating-Systems-Threads.org::#preemption][preemption]]?

*** CPU Scheduling
    :PROPERTIES:
    :CUSTOM_ID: drawing-scheduling
    :END:
    #+INDEX: Scheduling!Drawing (Scheduling)
    {{{revealimg("./figures/external-non-free/jvns.ca/scheduling.meta",t,"60rh")}}}

** Today’s Core Questions
   - How does the OS manage the shared resource CPU?  What goals are pursued?
   - How does the OS distinguish threads that could run on the CPU
     from those that cannot (i.e., that are blocked)?
   - How does the OS schedule threads for execution?

** Learning Objectives
   - Explain thread concept (continued)
     - Including states and priorities
   - Explain scheduling mechanisms and their goals
   - Apply scheduling algorithms
     - FCFS, Round Robin

** Retrieval Practice
   - Before you continue, answer the following; ideally, without
     outside help.
     - What is a process, what a thread?
     - What does concurrency mean?
       - How does it arise?
     - What is preemption?

*** Thread Terminology
#+REVEAL_HTML: <script data-quiz="quizThreadTerminology" src="./quizzes/thread-terminology.js"></script>

** Table of Contents
   :PROPERTIES:
   :UNNUMBERED: notoc
   :HTML_HEADLINE_CLASS: no-toc-progress
   :END:
#+REVEAL_TOC: headlines 1

* Scheduling
** CPU Scheduling
   :PROPERTIES:
   :CUSTOM_ID: cpu-scheduling
   :reveal_extra_attr: data-audio-src="./audio/4-scheduling-variants-1.ogg"
   :END:
   #+INDEX: Scheduling!Definition (Scheduling)
   #+INDEX: Scheduling!Preemption (Scheduling)
   #+INDEX: Preemption (Scheduling)
   - With multitasking, lots of threads *share resources*
     - Focus here: CPU
   - *Scheduling* (planning) and *dispatching* (allocation) of CPU via OS
     #+ATTR_REVEAL: :frag (appear)  :audio (./audio/4-scheduling-variants-2.ogg ./audio/4-scheduling-variants-3.ogg)
     - *Non-preemptive*, e.g., [[#fifo][FIFO scheduling]]
       - Thread on CPU until [[file:Operating-Systems-Threads.org::#yielding][yield]],
         termination, or [[file:Operating-Systems-Interrupts.org::#blocking][blocking]]
     - *Preemptive*, e.g., [[#roundrobin][Round Robin scheduling]]
       - Typical case for desktop OSs
         1. Among all threads, schedule and dispatch one, say T_0
         2. Allow T_0 to execute on CPU for some time, then preempt it
         3. Repeat, go to step (1)
   #+ATTR_REVEAL: :frag appear
   - (Similar decisions take place in industrial production, which you may know from
     operations management)
   #+begin_notes
Scheduling is the planning of resource allocations.  Here, we just
consider the allocation of the resource CPU among multiple threads.

Concerning wording, the planning itself is called *scheduling*, while
the allocation is called *dispatching*.  Thus, after making a scheduling
decision, the OS dispatches one thread to run on the CPU.

Two major scheduling variants are non-preemptive and preemptive ones.
With non-preemptive scheduling, the OS allows the currently executing
thread to continue as long as it wants.  The bullet point names some
situations when a thread might stop, which is when the next scheduling
decision takes place.

With preemptive scheduling, the OS may pause, or preempt, a thread in
the middle of its execution although it could continue with more
useful work on the CPU.  Here, the OS uses a timer to define the
length of some time slice, for which the dispatched thread is allowed
to run at most.  If the thread executes a blocking system call or
terminates before the timer runs out, the OS cancels the timer and
makes the next scheduling decision.  When the timer runs out, it
triggers an interrupt, causing the interrupt handler to run on the CPU
for the next scheduling decision.
   #+end_notes

** Sample Scheduling Goals
   - Scheduling is hard, as various goals with *trade-offs* exist
     - Trade-off: Improvement for one goal may negatively affect
       others (discussed subsequently)
   - Sample goals
     - Efficient resource usage
     - Fairness (e.g., equal CPU shares per thread)
     - Response time ([[#scheduling-goals][definition]])
     - Throughput ([[#scheduling-goals][definition]])

* Thread States
  :PROPERTIES:
  :CUSTOM_ID: sec-thread-states
  :END:

** Reasons to Distinguish States
   - Recall: Some threads may be blocked
     - E.g., wait for I/O operation to finish or ~sleep~ system call
       (recall [[file:Operating-Systems-Threads.org::#Simpler2Threads][Simpler2Threads]])
       - More generally, threads may perform [[file:Operating-Systems-Interrupts.org::#blocking][blocking]] system calls
     - [[file:Operating-Systems-Interrupts.org::#polling-pro-con][Busy waiting]]
       would be a waste of CPU resources
       - If other threads could run on CPU
   #+ATTR_REVEAL: :frag appear
   - OS distinguishes *thread states* (details subsequently)
     - Tell threads apart that might perform useful computations
       (runnable) on the CPU from those that do not (blocked/waiting)
     - Scheduler does not need to consider waiting threads
     - Scheduler considers runnable threads, selects one, dispatches
       that for execution on CPU (which is then running)

** OS Thread States
   :PROPERTIES:
   :CUSTOM_ID: thread-states
   :END:
   #+INDEX: Thread states!Definition (Scheduling)
   #+INDEX: Running thread (Scheduling)
   #+INDEX: Blocked thread (Scheduling)
   #+INDEX: Waiting thread (Scheduling)
   - Different OSs distinguish different sets of states; typically:
     - *Running*: Thread(s) currently executing on CPU (cores)
     - *Runnable*: Threads ready to perform computations
     - *Waiting* or *blocked*: Threads waiting for some event to occur
   #+ATTR_REVEAL: :frag appear
   - OS manages states via [[basic:https://en.wikipedia.org/wiki/Queue_(abstract_data_type)][queues]]
     (with suitable data structures)
     - *Run queue(s)*: Potentially per CPU core
       - Containing runnable threads, input for scheduler
     - *Wait queue(s)*: Potentially per event (type)
       - Containing waiting threads
         - OS inserts running thread here upon blocking system call
         - OS moves thread from here to run queue when event occurs

** Thread State Transitions
   :PROPERTIES:
   :CUSTOM_ID: state-transitions
   :reveal_extra_attr: data-audio-src="./audio/4-thread-states.ogg"
   :END:
   #+INDEX: Thread states!Transitions (Scheduling)
   {{{revealimg("./figures/OS/hail_f0303.pdf.meta",t,"50rh")}}}
#+BEGIN_NOTES
This diagram shows typical state transitions caused by actions of
threads, decisions of the OS, and external I/O events.  State changes
are always managed by the OS.

Newly created threads, such as the ones you created in Java, are Runnable.
When the CPU is idle, the OS’ scheduler executes a selection algorithm
among the Runnable threads and dispatches one to run on the CPU.  When
that thread yields or is preempted, the OS remembers that thread as
Runnable.

If the thread invokes a blocking system call, the OS changes its
state to Waiting.  Once the event for which the thread waits has
happened (e.g., a key pressed or some data has been transferred from
disk to RAM), the OS changes the state from Waiting to Runnable.
At some later point in time, that thread may be selected by the
scheduler to run on the CPU again.

In addition, an outgoing arc Termination is shown from state Running,
which indicates that a thread has completed its computations (e.g.,
the main function in Java ends).  Actually, threads may also be
terminated in states Runnable and Waiting, which is not shown here,
but which can happen if a thread is killed (e.g., you end a program or
shut down the machine).
#+END_NOTES

** Scheduling Vocabulary
#+REVEAL_HTML: <script data-quiz="quizSchedulingTerminology" src="./quizzes/scheduling-terminology.js"></script>


* Scheduling Goals
** Goal Overview
   :PROPERTIES:
   :CUSTOM_ID: scheduling-goals
   :reveal_extra_attr: data-audio-src="./audio/4-scheduling-goals.ogg"
   :END:
  - Performance
    - *Throughput*
      - Number of completed threads (computations, jobs) per time unit
      - More important for service providers than users
    - *Response time*
      - Time from thread start or interaction to useful reaction
  #+ATTR_REVEAL: :frag appear :audio ./audio/4-scheduling-goals-2.ogg
  - User control
    - *Resource allocation*
    - Mechanisms for urgency or importance, e.g., *priorities*
  #+begin_notes
As computer users, we expect different goals from scheduling
mechanisms, for which subsequent slides contain some details: First,
we are usually interested in high performance in the senses of
throughput and response time.

Second, we may want to exert some control to influence the scheduling
decisions.  For example, when you think of rented compute capacity,
where you share resources with other customers, the resources
allocated to you (including CPU time) depend on the amount of money
you pay.

Besides, programmers can assign priorities to threads to indicate
their relative urgency or importance.
  #+end_notes

*** Throughput
    - To increase throughput, avoid idle times of CPU
      - Thus, reassign CPU when currently running thread needs to wait
      - Context switching necessary
    - Recall: Context switching comes with [[file:Operating-Systems-Threads.org::#multitasking-overhead][overhead]]
      - Overhead reduces throughput

*** Response Time
    - Frequent context switches may help for small response time
      - However, their overhead hurts throughput
    - Responding quickly to one thread may slow down another one
      - May use priorities to indicate preferences

*** Resource Allocation
    - What fraction of resources for what purpose?
    #+ATTR_REVEAL: :frag appear
    - *Proportional share scheduling*
      - E.g., multi-user machine: Different users obtain same share
        of CPU time every second
        - (Unless one pays more: Larger share for that user)
    #+ATTR_REVEAL: :frag appear
    - *Group scheduling*: Assign threads to groups, each of which
      receives its proportional share
      @@html:<br>@@ → Linux scheduler [[#cfs][later on]]

** Priorities in Practice
   :PROPERTIES:
   :CUSTOM_ID: priority-practice
   :END:
   #+INDEX: Priority (Scheduling)
   #+INDEX: Thread!Priority (Scheduling)
   - Different OSs (and execution environments such as Java) provide
     different means to express priority
     - E.g., numerical priority, so-called niceness value, deadline, …
     - Upon thread creation, its priority can be specified (by the
       programmer, with default value)
       - Priority recorded in [[file:Operating-Systems-Threads.org::#tcb][TCB]]
       - Sometimes, administrator privileges are necessary for “high”
         priorities
       - Also, OS tools may allow to change priorities at runtime

** Thread Properties in Java
   :PROPERTIES:
   :CUSTOM_ID: java-thread-properties
   :END:
   #+INDEX: Java thread properties (Scheduling)
   - [[https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/lang/Thread.html][Java API]]
     - “Every thread has a priority. Threads with higher priority are executed in preference to threads with lower priority.”
     - May interpret as: Preemptive, [[#fixed-priority][priority-driven]] scheduling
   - Priorities via integer values
     - Higher number → more CPU time
     - Preemptive
       - Current thread has highest priority
       - Newly created thread with higher priority replaces old one on CPU
       - Most of the time (above quote from API is vague)
   - Time slices not guaranteed (implementation dependent)
     - [[#priority-starvation][Starvation]] possible


* Scheduling Mechanisms
** Three Families of Schedulers
#+BEGIN_leftcol
- This section covers three families of schedulers
  1. [[#fixed-priority][Fixed thread priorities]]
  2. [[#dynamic-priority][Dynamically adjusted thread priorities]]
  3. [[#proportional-share][Controlling proportional shares of processing time]]
- Visualization on right shows structure of this section
  - With sample algorithms per family
#+END_leftcol
#+BEGIN_rightcol
{{{reveallicense("./figures/OS/4-scheduling-mechanisms.meta","50rh",nil,none)}}}
#+END_rightcol

*** Notes on Scheduling
    - For scheduling with pen and paper, you need to know
      *arrival times* and *service times* for threads
      - Arrival time: Point in time when thread created
      - Service time: CPU time necessary to complete thread
        - (For simplicity, blocking I/O is not considered; otherwise,
          you would also need to know frequency and duration of I/O
          operations)
    #+ATTR_REVEAL: :frag appear
    - OS does not know either ahead of time
      - OS creates threads (so, knowledge of arrival time is not an
        issue) and inserts them into necessary data structures during
        normal operation
      - When threads terminate, OS again participates
        - Thus, OS can compute service time after the fact
        - (Some scheduling algorithms require service time for
          scheduling decisions; then threads need to declare that upon
          start.  Not considered here.)

** Fixed-Priority Scheduling
   :PROPERTIES:
   :CUSTOM_ID: fixed-priority
   :END:
   #+INDEX: Scheduling!Fixed-priority (Scheduling)
   #+INDEX: Fixed-priority scheduling (Scheduling)
   - Use *fixed*, numerical *priority* per thread
     - Threads with higher priority preferred over others
       - Smaller or higher numbers may indicate higher priority:
         OS dependent
   #+ATTR_REVEAL: :frag appear
   - Implementation alternatives
     - Single queue ordered by priority
     - Or one queue per priority
       - OS schedules threads from highest-priority non-empty queue
   #+ATTR_REVEAL: :frag appear
   - Scheduling whenever CPU idle or some thread becomes runnable
     - Dispatch thread of highest priority
       - In case of ties: Run one until end ([[#fifo][FIFO]]) or
         serve all [[#roundrobin][Round Robin]]

*** Warning on Fixed-Priority Scheduling
    :PROPERTIES:
    :CUSTOM_ID: priority-starvation
    :END:
    #+INDEX: Starvation (Scheduling)
    - *Starvation* of low-priority threads possible
      - Starvation = continued denial of resource
        - Here, low-priority threads do not receive resource CPU as
          long as threads with higher priority exist
      - Careful design necessary
        - E.g., for hard-real-time systems (such as cars, aircrafts,
          power plants)
        - (Beware of [[file:Operating-Systems-MX-Challenges.org::#priority-inversion][priority inversion]],
          a topic for a later presentation!)

*** FIFO/FCFS Scheduling
    :PROPERTIES:
    :CUSTOM_ID: fifo
    :END:
    #+INDEX: Scheduling!FIFO (Scheduling)
    #+INDEX: FIFO scheduling (Scheduling)
    - FIFO = First in, first out
      - (= FCFS = first come, first served)
      - Think of queue in supermarket
    - Non-preemptive strategy: Run first thread until completed (or blocked)
      - For threads of equal priority

*** Round Robin Scheduling
    :PROPERTIES:
    :CUSTOM_ID: roundrobin
    :END:
    #+INDEX: Scheduling!Round robin (Scheduling)
    #+INDEX: Round robin scheduling (Scheduling)
    #+INDEX: Time slice (Scheduling)
    #+INDEX: Quantum (Scheduling)
    #+INDEX: Queue (Scheduling)
    - Key ingredients
      - *Time slice* (quantum, q)
        - Timer with interrupt, e.g., every 30ms
      - *Queue(s)* for runnable threads
        - Newly created thread inserted at end
      #+ATTR_REVEAL: :frag appear
      - Scheduling when (1) timer interrupt triggered or (2) thread ends
        or is blocked
        #+ATTR_REVEAL: :frag (appear)
        1. Timer interrupt: *Preempt* running thread
           - Move previously running thread to end of runnable queue (for
             its priority)
           - Dispatch thread at head of queue (for highest priority) to CPU
             - With new timer for full time slice
        2. Thread ends or is blocked
           - Cancel its timer, dispatch thread at head of queue (for full
             time slice)
    #+ATTR_REVEAL: :frag appear
    - Video tutorial in {{{learnweb}}}


** Dynamic-Priority Scheduling
   :PROPERTIES:
   :CUSTOM_ID: dynamic-priority
   :END:
   #+INDEX: Scheduling!Dynamic-priority (Scheduling)
   #+INDEX: Dynamic-priority scheduling (Scheduling)
   - With dynamic strategies, OS can adjust thread priorities during execution
   - Sample strategies
     - *Earliest deadline first*
       - For tasks with deadlines — discussed in cite:Hai19, not part of
         learning objectives
     - *Decay Usage Scheduling*

*** Decay Usage Scheduling
    :PROPERTIES:
    :CUSTOM_ID: decay-usage
    :END:
    #+INDEX: Scheduling!Decay usage (Scheduling)
    #+INDEX: Decay usage scheduling (Scheduling)
    - General intuition:
      [[file:Operating-Systems-Threads.org::#classification][I/O bound]]
      threads are at unfair disadvantage.  (Why?)
      #+ATTR_REVEAL: :frag appear
      - *Decrease* priority of threads in *running state*
      - *Increase* priority of threads in *waiting state*
        - Allows quick reaction to I/O completion (e.g, user interaction)
    #+ATTR_REVEAL: :frag appear
    - OS may manage one queue of threads per priority
      - Threads move between queues when priorities change
        - Falls under more general pattern of [[beyond:https://en.wikipedia.org/wiki/Multilevel_feedback_queue][multilevel feedback queue]]
          schedulers
    - Technically, threads have a *base priority* that is adjusted by OS
      - Use [[#roundrobin][Round Robin scheduling]] (for non-empty
        queue of highest-priority threads) after priority adjustments

*** Decay Usage Scheduling in Mac OS X
    - OS keeps track of CPU *usage*
      - Usage increases linearly for time spent on CPU
        - Usage recorded when thread leaves CPU (yield or preempt)
      - Usage *decays* exponentially while thread is waiting
    - Priority adjusted downward based on CPU usage
      - Higher adjustments for threads with higher usage
        - Those threads’ priorities will be lower than others

*** Variant in MS Windows
    - Increase of priority when thread leaves waiting state
      - Priority *boosting*
      - Amount of boost depends on wait reason
        - More for interactive I/O (keyboard, mouse) then other types
    - After boost, priority decreases linearly with increasing CPU
      usage

** Proportional-Share Scheduling
   :PROPERTIES:
   :CUSTOM_ID: proportional-share
   :END:
   #+INDEX: Scheduling!Proportional-share (Scheduling)
   #+INDEX: Scheduling!Weighted round robin (Scheduling)
   #+INDEX: Scheduling!Weighted fair queuing (Scheduling)
   #+INDEX: Proportional-share scheduling (Scheduling)
   #+INDEX: Weighted round robin (WRR) (Scheduling)
   #+INDEX: Weighted fair queuing (WFQ) (Scheduling)
   - Simple form: *Weighted* [[#roundrobin][Round Robin]] (WRR)
     - Weight per thread is factor for length of time slice
     - Discussion
       - Con: Threads with high weight lead to long delays for others
       - Pro: Fewer context switches than following alternative
       - (See [[#wrr-vs-wfq][next slide]])
   #+ATTR_REVEAL: :frag appear
   - Alternative: *Weighted fair queuing* (WFQ)
     - Uniform time slice
     - Threads with lower weight “sit out” some iterations

*** WRR vs WFQ with sample Gantt Charts
    :PROPERTIES:
    :CUSTOM_ID: wrr-vs-wfq
    :END:
    - Threads T1, T2, T3 with weights 3, 2, 1; service times > 30ms; q = 10ms
      - Supposed order of arrival: T1 first, T2 second, T3 third
      - If threads are not done after shown sequence, start over with T1

    {{{reveallicense("./figures/OS/4-wrr.meta","10rh",nil,t)}}}

    {{{reveallicense("./figures/OS/4-wfq.meta","10rh",nil,t)}}}

** CFS in Linux
   :PROPERTIES:
   :CUSTOM_ID: cfs
   :END:
   #+INDEX: Scheduling!Completely fair (Scheduling)
   #+INDEX: Completely fair scheduler (CFS) (Scheduling)
   - CFS = Completely fair scheduler
     - Actually, variant of [[#proportional-share][WRR]] above
       - Weights determined via so-called *niceness* values
         - (Lower niceness means higher priority)
   - *Core idea*
     #+ATTR_REVEAL: :frag (appear)
     - Keep track of how long threads were on CPU
       - Scaled according to weight
       - Time spent on CPU called *virtual runtime*
         - Represented efficiently via [[beyond:https://en.wikipedia.org/wiki/Red%E2%80%93black_tree][red-black tree]]
     - Schedule thread that is furthest behind
       - This thread received least amount of CPU time → allow to
         catch up
       - Until preempted or time slice runs out
     - (Some details in cite:Hai19)

** When to Schedule
   :PROPERTIES:
   :CUSTOM_ID: when-scheduling
   :reveal_extra_attr: data-audio-src="./audio/4-scheduling-events.ogg"
   :END:
   #+INDEX: Scheduling!When to schedule (Scheduling)
   - Unless explicitly specified otherwise, we consider *preemptive*
     scheduling with *time slices* and *priorities*
     - Threads may be removed from CPU before they are “done”
       - As with Linux kernel, under stricter interpretation than
         [[#java-thread-properties][Java’s]]:
         - "[[https://man7.org/linux/man-pages/man7/sched.7.html][All
           scheduling is preemptive: if a thread with a higher static
           priority becomes ready to run, the currently running thread
           will be preempted and returned to the wait list for its
           static priority level.]]"
   - Scheduling based on thread states, priorities, and time slices
     - Sample *events* that may initiate scheduling
       - Thread state(s) change
         - E.g., thread created or finished, blocking system call, I/O finished; later:
           [[file:Operating-Systems-MX.org::#locks-mutexes][(un-) locking]]
       - Thread priorities change
       - Time slice runs out
   #+begin_notes
Let us revisit /when/ scheduling takes place.  It turns out that
precise answers depend on design and implementation decisions.
We consider /preemptive/ scheduling of threads where /time slicing/
enables multitasking; concerning /priorities/, we suppose that they
are handled with preemption as in the case of Linux, for which a quote
from a ~man~ page is shown on the slide.

Importantly, the scheduler only considers /runnable/ threads.  Thus,
state changes may require a scheduling decision.  E.g., if a thread
invokes a blocking systems call, some other thread needs to be
selected to run.  Similarly, when an event occurs that makes one or
more previously blocked threads runnable, scheduling /may/ happen:
If a newly runnable thread has highest priority, the currently running
one is preempted and scheduling takes place to select a new one;
otherwise, the currently running thread is likely to continue (but
there may be implementations that disagree).

Similarly, if the priority of the currently running thread decreases
below that of other threads or if some other thread or threads gain
highest priority, scheduling must take place.  In contrast, the
creation of new threads with low priority does not require scheduling.

Clearly, preemptive scheduling also takes place when the OS believes
that the current thread ran long enough.

Be careful not to confuse interrupt processing with scheduling.
Indeed, some of the above events involve interrupts while others do
not: What matters are /events/ with relevance to scheduling as just
discussed; whether an interrupt was involved in an event is less
important.
   #+end_notes

** JiTT Task for Scheduling
    :PROPERTIES:
    :reveal_data_state: no-toc-progress
    :reveal_extra_attr: class="jitt"
    :END:

    This task is available for self-study in {{{jittquiz}}}.

    Perform Round Robin scheduling given the following situation:

       | q=4 | Thread | Arrival Time | Service Time |
       |-----+--------+--------------+--------------|
       |     | T1     |            0 |            3 |
       |     | T2     |            1 |            6 |
       |     | T3     |            4 |            3 |
       |     | T4     |            9 |            6 |
       |     | T5     |           10 |            2 |



* Pointers beyond Class Topics
** Fair Queuing
   :PROPERTIES:
   :CUSTOM_ID: fair-queuing
   :END:
   #+INDEX: Fair queuing (Scheduling)
   - *Fair queuing* is a general technique
     - Also used, e.g., in computer networking
   - Intuition
     - System is *fair* if at all times every party has
       progressed according to its share
       - This would require infinitesimally small steps
   - Reality
     - Approximate fairness via “small” discrete steps
     - E.g., [[#cfs][CFS]]

*** CFS with Blocking
    - Above description of [[#cfs][CFS]] assumes runnable threads
    - Blocked threads lag behind
      - If blocked briefly, allow to catch up
      - If blocked for a long time (above threshold), they would
        deprive all other threads from CPU once awake again
        - Hence, counter-measure necessary
          - Give up fairness
        - Forward virtual runtime to be slightly less than minimum of
          previously runnable threads
    - Effect similar to dynamic priority adjustments of decay usage
      schedulers

*** CFS with Groups
    - CFS allows to assign threads to (hierarchies of) groups
      - Scheduling then aims to treat groups fairly
    - For example
      - One group per user
        - Every user obtains same CPU time
      - User-defined groups
        - E.g., multimedia with twice as much CPU time as programming
          (music and video running smoothly despite compile jobs)

* Conclusions
** Summary
   - OS performs scheduling for shared resources
     - Focus here: CPU scheduling
     - Subject to conflicting goals
   - CPU scheduling based on thread states and priorities
     - Fixed vs dynamic priorities vs proportional share
     - CFS as example for proportional share scheduling

#+INCLUDE: "backmatter.org"
