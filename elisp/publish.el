;;; publish.el --- Publish reveal.js presentation from Org files
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Code:
;; Avoid update of emacs-reveal, enable stacktraces.
(setq emacs-reveal-managed-install-p nil
      debug-on-error t)

;; Set up load-path.
(let ((install-dir
       (mapconcat #'file-name-as-directory
                  `(,user-emacs-directory "elpa" "emacs-reveal") "")))
  (add-to-list 'load-path install-dir)
  (condition-case nil
      ;; Either require package with above hard-coded location
      ;; (e.g., in docker) ...
      (require 'emacs-reveal)
    (error
     ;; ... or look for sibling "emacs-reveal".
     (add-to-list
      'load-path
      (expand-file-name "../../emacs-reveal/" (file-name-directory load-file-name)))
     (require 'emacs-reveal))))

;; Decide later whether to use minted.  Needs python-pygments in Docker image.
;; (require 'ox-latex)
;; (setq org-latex-listings 'minted)
;; (add-to-list 'org-latex-packages-alist '("newfloat" "minted"))

;; Make an index.  Do not delay for warnings.
(setq oer-reveal-publish-makeindex t
      oer-reveal-warning-delay nil)

;; Make sure that links to local files are correct in PDF version.
(defun oer-reveal-latex-link-filter (href backend _)
  "If BACKEND is LaTeX and HREF links to org file, replace with pdf version.
Based on suggestion by Tim Cross, see URL
`https://lists.gnu.org/archive/html/emacs-orgmode/2022-06/msg00368.html'."
  (when (org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "\\.org\\}" ".pdf}" href)))
(add-to-list 'org-export-filter-link-functions
             'oer-reveal-latex-link-filter)

;; Publish reveal.js presentations.
(oer-reveal-publish-all
 (list
  (list "texts"
       	:base-directory "texts"
       	:base-extension "org"
       	:publishing-function '(oer-reveal-publish-to-pdf)
       	:publishing-directory "./public/texts")
  (list "orgs"
       	:base-directory "org-resources"
       	:base-extension "org"
       	:publishing-function 'org-publish-attachment
       	:publishing-directory "./public/org-resources")
  (list "index-terms"
	:base-directory "."
	;; A file theindex.org (which includes theindex.inc)
	;; is created due to assignment to
	;; oer-reveal-publish-makeindex above.
	;; Using that setting, the file is automatically published with
	;; org-re-reveal, which is useless.
	;; Thus, publish as HTML here.  For this to work, index-terms.org
	;; is a manually created file including theindex.inc.
	;; The preparation and completion functions below set up an
	;; advice on org-html-link to rewrite links into presentations
	;; using org-re-reveal's anchor format.
	:include '("index-terms.org")
	:exclude ".*"
	:preparation-function 'oer-reveal--add-advice-link
	:completion-function 'oer-reveal--remove-advice-link
	:publishing-function '(org-html-publish-to-html)
	:publishing-directory "./public")
  (list "images"
	:base-directory "img"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/img"
	:publishing-function 'org-publish-attachment
	:recursive t)
  (list "videos"
	:base-directory "videos"
	:base-extension (regexp-opt '("mp4"))
	:publishing-directory "./public/videos"
	:publishing-function 'org-publish-attachment
	:recursive t)
  (list "title-logos"
	:base-directory "non-free-logos/title-slide"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/title-slide"
	:publishing-function 'org-publish-attachment)
  (list "theme-logos"
	:base-directory "non-free-logos/reveal-css"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/reveal.js/css/theme"
	:publishing-function 'org-publish-attachment)
  (list "eval"
	:base-directory "eval"
	:base-extension (regexp-opt '("ods" "csv"))
	:publishing-directory "./public/eval"
	:publishing-function 'org-publish-attachment)
  (list "redirect"
	:base-directory "redirect"
	:base-extension 'any
	:publishing-directory "./public"
	:publishing-function 'org-publish-attachment)
  ))
;;; publish.el ends here
