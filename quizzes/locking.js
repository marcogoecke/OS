quizLocking = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Locking Basics",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements related to Mutual Exclusion.",
            "a": [
                {"option": "A lock can be in either of two states.", "correct": true},
                {"option": "A lock in state locked cannot be locked.", "correct": true},
                {"option": "If lock() is invoked on a lock in state locked, the executing thread needs to wait.", "correct": true},
                {"option": "“Waiting” for locks can happen in two forms: busy waiting or blocking.", "correct": true},
                {"option": "Locks are shared resources.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: Most or all statements are correct, see <a href=\"Operating-Systems-MX.html#slide-locks-mutexes\">this earlier slide</a>.)</span> Please write down arguments in favor of your view and discuss online.</p>" // no comma here
        }
    ]
};
