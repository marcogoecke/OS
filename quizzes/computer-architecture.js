quizComputerArchitecture = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Von Neumann and Hack",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about the von Neumann architecture",
            "a": [
                {"option": "Instructions and data reside in separate memories.", "correct": false},
                {"option": "Instructions cannot modify other instructions.", "correct": false},
                {"option": "Memory consists of numbered locations for fixed-sized words.", "correct": true},
                {"option": "To access a memory location, its number needs to be placed on a bus.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 2 statements are correct.)</span> Maybe check out <a href=\"https://en.wikipedia.org/wiki/Von_Neumann_architecture\">Wikipedia</a>?</p>" // no comma here
        },
        {
            "q": "Select correct statements about the Hack architecture",
            "a": [
                {"option": "Instructions and data reside in separate memories.", "correct": true},
                {"option": "Instructions cannot modify other instructions.", "correct": true},
                {"option": "Memory consists of numbered locations, where words have a size of 2 bytes.", "correct": true},
                {"option": "When a key is pressed, the CPU is notified about this event.", "correct": false},
                {"option": "Each pixel on screen is represented by one bit in some reserved memory range.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: Most statements are correct.)</span> Please, write <a href=\"https://www.nand2tetris.org/project04\">Fill.asm</a> and retry.</p>" // no comma here
        }
    ]
};
