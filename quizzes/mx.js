quizMX = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "MX Vocabulary",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements related to Mutual Exclusion.",
            "a": [
                {"option": "Mutual exclusion should be enforced for critical sections.", "correct": true},
                {"option": "Critical sections must be identified by programmers.", "correct": true},
                {"option": "Start and end of critical sections might be indicated with special operations on locks or semaphores.", "correct": true},
                {"option": "A lock in Java is just a special object.", "correct": true},
                {"option": "Proper locking and unlocking of mutexes is the programmer’s responsibility.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: Most or all statements are correct.)</span> Please check out earlier slides, e.g., <a href=\"Operating-Systems-MX.html#slide-mx-goal\">goal of MX</a> and <a href=\"Operating-Systems-MX.html#slide-locks-mutexes\">slides on locking</a>.</p>" // no comma here
        }
    ]
};
