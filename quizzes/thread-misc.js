quizThreadMisc = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Facts about Threads",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select the correct statement related to Threads.",
            "a": [
                {"option": "When a thread is preempted, the OS changes its state to blocked.", "correct": false},
                {"option": "When round-robin scheduling is used, starvation of threads is avoided.", "correct": false},
                {"option": "When the OS changes a high-priority thread from blocked to runnable, another thread might be preempted.", "correct": true},
                {"option": "A thread attempting to lock a locked spinlock gets blocked.", "correct": false}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: One statement is correct.)</span> Please write down arguments in favor of your view and discuss online.</p>" // no comma here
        },
        {
            "q": "Select the correct statement related to synchronized.",
            "a": [
                {"option": "No two Java threads can execute a synchronized method in parallel.", "correct": false},
                {"option": "A synchronized method gets locked when a thread enters it.", "correct": false},
                {"option": "When a thread fails to acquire a lock for execution of synchronized code, it busily waits until that lock is released.", "correct": false},
                {"option": "When a thread enters a synchronized method, it automatically tries to acquire a lock.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: One statement is correct.)</span> With synchronized, what gets locked? Note that multiple threads may lock different things. What executions does this prevent? Maybe write down arguments in favor of your view and discuss online.</p>" // no comma here
        }
    ]
};
