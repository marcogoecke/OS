quizBigPictureVM = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "A bird's view on memory",
        "level1":  "Excellent!",          // 80-100%
        "level2":  "Please re-try.",      // 60-79%
        "level3":  "Please re-try.",      // 40-59%
        "level4":  "Maybe ask for help?", // 20-39%
        "level5":  "Please ask for help." //  0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about virtual memory.",
            "a": [
                {"option": "Virtual memory includes RAM and secondary storage areas.", "correct": true},
                {"option": "Each process has its own virtual address space, starting at virtual address 0.", "correct": true},
                {"option": "Virtual address spaces are split into frames.", "correct": false},
                {"option": "Pages and frames share the same size, which is necessary as pages are loaded into frames.", "correct": true}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: Most or all statements are correct.) Please revisit the <a href=\"Operating-Systems-Memory-I.html#slide-big-picture\">big picture</a> of virtual memory.</span></p>" // no comma here
        },
        {
            "q": "Select correct statements about virtual memory.",
            "a": [
                {"option": "When a process starts, the OS loads all its pages into frames.", "correct": false},
                {"option": "The OS records for each process which pages are located in what frames.", "correct": true},
                {"option": "It is the programmer's task to load necessary pages into frames before data can be accessed.", "correct": false},
                {"option": "Machine instructions refer to virtual addresses.", "correct": true},
                {"option": "The size of RAM is a limit for the size of virtual address spaces.", "correct": false}
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 2 statements are correct.) Please revisit the <a href=\"Operating-Systems-Memory-I.html#slide-big-picture\">big picture</a> of virtual memory.</span></p>" // no comma here
        }
    ]
};
